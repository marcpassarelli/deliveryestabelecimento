import {db, auth} from './firebase'
import firebase from 'firebase'

messageRef = null;
export var pedidos =[]
export var pedidosHistorico=[]
export var confirmadoRecebimento=[]

export async function login (email, pass, onLogin,onError) {

    try {
      console.log("dentro do try");
        await auth
            .signInWithEmailAndPassword(email, pass);
            onLogin()

    } catch (error) {
      console.log("dentro do catch"+JSON.stringify(error));
        var errorCode = error.code
        if (errorCode === 'auth/wrong-password' || errorCode === 'auth/invalid-email' ){
          alert('Credenciais Incorretas');
          onError()
        }
        else if (errorCode === 'auth/user-disabled'){
          alert('Usuário desabilitado')
          onError()
        }else if (errorCode === 'auth/user-not-found'){
          alert('Usuário não cadastrado')
          onError()
        }
    }

}

export async function signup (email, pass, onSignup) {

    try {
        await auth
            .createUserWithEmailAndPassword(email, pass);

        onSignup()
        // Navigate to the Home page, the user is auto logged in

    } catch (error) {
      var errorCode = error.code
      if (errorCode === 'auth/email-already-in-use'){
        alert('Já existe uma conta com esse email');
      }
      else if (errorCode === 'auth/invalid-email'){
        alert('Email inválido')
      }
    }
}

export async function logout() {

    try {

        await auth.signOut();

        // Navigate to login view

    } catch (error) {
        console.log(error);
    }

}

export function open(){

}


export function loadMessages(estabelecimento,onListLoad){
  db.ref("/messages/"+estabelecimento).once('value').then(function(snapshot){
    console.log("snapshotloadmessages"+snapshot);
    pedidosHistorico=[]
    snapshot.forEach((child)=>{
        pedidosHistorico.push({
          _id: child.key,
          idUser: child.val().idUser,
          carrinho: child.val().carrinho,
          createdAt: child.val().createdAt,
          formaPgto: child.val().formaPgto,
          formaPgtoDetalhe: child.val().formaPgtoDetalhe,
          retirar:child.val().retirar,
          nome: child.val().nome,
          telefone: child.val().telefone,
          endereco: child.val().endereco,
          bairro: child.val().bairro,
          referencia: child.val().referencia,
          frete:child.val().frete,
          total:child.val().total,
          status: child.val().status
        })
    })
    onListLoad()
  })
}

export function preparandoMessages(estabelecimento,onLoad){
  db.ref("/messages/"+estabelecimento).once('value').then(function(snapshot){
    confirmadoRecebimento=[]
    snapshot.forEach((child)=>{

      if(child.val().status=="Confirmado Recebimento"){
        let frete =0
        // console.log("dentro do if preparando mensagens"+JSON.stringifychild.val().carrinho);
        if(child.val().retirar==true){
          frete = 0
        }else if(child.val().retirar==false){
          frete = child.val().frete
        }
        confirmadoRecebimento.push({
          token:child.val().tokenUser,
          _id: child.key,
          carrinho: child.val().carrinho,
          createdAt: new Date(child.val().createdAt),
          formaPgto: child.val().formaPgto,
          formaPgtoDetalhe: child.val().formaPgtoDetalhe,
          retirar:child.val().retirar,
          nome: child.val().nome,
          telefone: child.val().telefone,
          endereco: child.val().endereco,
          bairro: child.val().bairro,
          referencia: child.val().referencia,
          frete:frete,
          total:child.val().total,
          status: child.val().status
        })
      }
    })
    onLoad()
  })
}

export var numChildren=0;



export function waitMessages(estabelecimento,callback){

  this.messageRef = db.ref("/messages/"+estabelecimento)
  this.messageRef.on('child_added', function(snapshot){
    const message = snapshot.val()
    var atual = new Date()
    // console.log("numChildren"+numChildren);
    var diferenca = atual - message.createdAt

    if(message.status=="Aguardando Confirmação"){
      var novo;
      if(diferenca<60000){
        novo = true
      }else{
        novo = false
      }
      callback({
        token:message.tokenUser,
        _id: snapshot.key,
        idUser: message.idUser,
        carrinho: message.carrinho,
        createdAt: new Date(message.createdAt),
        formaPgto: message.formaPgto,
        formaPgtoDetalhe: message.formaPgtoDetalhe,
        nome: message.nome,
        telefone: message.telefone,
        endereco: message.endereco,
        bairro: message.bairro,
        referencia: message.referencia,
        status: message.status,
        frete:message.frete,
        retirar:message.retirar,
        total:message.total,
        new:novo
      })
    }
  });

}

export function removeMessage(estabelecimento,callback){

  this.messageRef = db.ref("/messages/"+estabelecimento)
  this.messageRef.on("child_removed", function(snapshot) {
    var deletedPost = snapshot.key;
    callback({
      deletedPost:deletedPost
    })
  })
}

export function salvarPedidoOcioso(estabelecimento,pedido){

    this.messageRef = db.ref("/messages/"+estabelecimento+"/falhaOcioso/")
    this.messageRef.push({
      pedido
    })
}



export function updateStatus(estabelecimento,chave,userId,status,callback) {

  if(userId=='semCadastro'){

    this.messageRef = db.ref("/messages/"+estabelecimento+"/"+chave)
    this.messageRef.off()
    this.messageRef.update({status:status})
    this.messageRef.once('value').then(function(snapshot){
      callback({status:snapshot.val().status})
    })
  }else{

    this.messageRef = db.ref("/messages/"+estabelecimento+"/"+chave)
    this.historicoPedidos = db.ref("/user/"+userId+"/details/pedidos/"+chave+"/")
    this.messageRef.off()
    this.historicoPedidos.off()

    this.messageRef.update({status:status})
    .then(this.historicoPedidos.update({status:status}))
    .then((function(snapshot){
          console.log("snapshot status "+snapshot);
          callback({status:status})
    }))

  }
}

export function setDate(estabelecimento,createdAt,chave){
  console.log("createdAt"+createdAt);
  var date = new Date(createdAt)
  console.log("date: "+date);
  this.messageRef = db.ref("/messages/"+estabelecimento+"/"+chave+"/")
  console.log("camim setDate"+this.messageRef);
  this.messageRef.update({data:date}).then(function(snap){
    console.log("inserimo a data carai"+snap);
  },function(error){
    console.log("error de data"+error);
  })
}

export function updateStatusUser(userId,key,status){
    try {
      this.historicoPedidos = db.ref("/user/"+userId+"/details/pedidos/"+key+"/")
      this.historicoPedidos.off();
      this.historicoPedidos.update({status:status})
    } catch (e) {
      console.log("error"+e);
    }
}

export async function salvarPedidoUser(userId, key, pedido,onCompletion){
    try {
      this.historicoPedidos = db.ref("/user/"+userId+"/details/pedidos/"+key+"/")
      this.historicoPedidos.off();
      this.historicoPedidos.set(pedido)
      this.historicoPedidosCreatedAt = db.ref("/user/"+userId+"/details/pedidos/"+key+"/createdAt/")
      this.historicoPedidosCreatedAt.off();
      this.historicoPedidosCreatedAt.set(firebase.database.ServerValue.TIMESTAMP)
      this.historicoPedidos.off();
      this.historicoPedidosCreatedAt.off();
      onCompletion()
    } catch (e) {
      console.log("error"+e);
    }

}

export async function checkPedidoUser(userId, key, pedido,callback){
    try {

      this.pedido = db.ref("/user/"+userId+"/details/pedidos/"+key+"/")
      this.pedido.off()
      this.pedido.once('value').then(function(snapshot){
        console.log("snapshot"+JSON.stringify(snapshot));
        if(snapshot){
          callback({
            temPedido: true
            })
        }else{
          callback({
            temPedido: false
            })
        }
      })
    } catch (e) {
      console.log("error"+e);
    }

}

export function checkUpdateStatusUser(userId,key,status,onFailure,onSuccess){
    try {
      db.ref("/user/"+userId+"/details/pedidos/"+key+"/status/").once('value').then(function(snapshot){

          console.log("snapshot.val()"+snapshot.val());
          if(snapshot.val()==status){
            console.log("success");
            onSuccess()
          }else{

            console.log("failure");
            onFailure()
          }
          // if(snapshot.val()==status){
          //   onCompletion()
          // }else{
          //   this.historicoPedidos.update({status:status})
          // }

      })

    } catch (e) {
      console.log("error"+e);
    }

}

export function updateStatusSemItem(estabelecimento,chave,status,itemIndisponivel,callback) {
  this.messageRef = db.ref("/messages/"+estabelecimento+"/"+chave)
  this.messageRef.off()
  this.messageRef.update({status:status})
  this.messageRef.off()

  this.messageRef = db.ref("/messages/"+estabelecimento+"/"+chave+"/itemIndisponivel")
  this.messageRef.off()

  this.messageRef.push({nome:itemIndisponivel})
}

export function getEstabName(estabID,callback){
  db.ref("/estabelecimentosID/"+estabID+"/").once('value').then(function(snapshot){
    callback({
      nome:snapshot.val().nome,
      tipo:snapshot.val().tipo})
  })
}

export function getTokenDB(estab, callback){
  db.ref("/infoEstabelecimentosNovo/"+estab+"/token").once('value').then(function(snapshot){
    var token = snapshot.val()
    if(token){
      callback({
        token:token})
    }else {
      callback({token:"token"})
    }
  })
}

export function setToken(estab, token){
  db.ref("/infoEstabelecimentosNovo/"+estab+"/token/lista").push({token: token})
  // db.ref("/infoEstabelecimentosNovo/"+estab+"/token").update({token})

}

export function horarioFuncionamento(estabelecimento,dayOfWeek,callback){

  try{
    db.ref("/infoEstabelecimentosNovo/"+estabelecimento+"/horarioFuncionamento/"+dayOfWeek+"/").once('value').then(function(snapshot){

    callback({abertura:snapshot.val().abertura,
              fechamento:snapshot.val().fechamento})
  })
}catch(error){
    console.log("error"+error);
  }

}

// export function horarioFechamento(estabelecimento,dayOfWeek,callback){
//
//   try{
//     db.ref("/infoEstabelecimentos/"+estabelecimento+"/horarioFuncionamento/"+dayOfWeek+"/fechamento").once('value').then(function(fechamento){
//     callback({fechamento})
//   })
// }catch(error){
//     console.log("error"+error);
//   }
//
// }
//
// export function openEstabelecimento(tipoEstab,nomeEstab){
//   db.ref("/tiposEstabelecimentos/"+)
//
// }
