console.ignoredYellowBox = [
    'Setting a timer'
]

import React, { Component } from 'react';
import { ImageBackground, Image, Alert, Text, View, TouchableOpacity,Platform } from 'react-native'
import LazyYellowButton from '../constants/lazyYellowButton'
import LazyActivity from '../constants/lazyActivity'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { styles, images, cores } from '../constants/constants'
import { logout } from '../firebase/database'
import { auth} from '../firebase/firebase'
import firebase from 'react-native-firebase';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import _ from 'lodash';
import config from './config';

var count =0
export class ProfileScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nomeEstab:'',
      loading:false
    };
  }

  static navigationOptions = {
     header: null,
     tabBarLabel: 'MEU PERFIL',
     // Note: By default the icon is only shown on iOS. Search the showIcon option below.
     tabBarIcon: ({ tintColor }) => (
     <Image
       source={require('../../img/cadastro.png')}
       style={[styles.icon]}
     />
   ),
  };

  componentWillMount(){
    const {state} = this.props.navigation;
    let nomeEstab = state.params ? state.params.nomeEstab : ""
    this.setState({
      nomeEstab: nomeEstab
    });
  }

  componentDidMount = () => {

}

  goToLogin = () =>
  {
    const {navigate} = this.props.navigation;
    navigate('Login')
  }

  goToHistoricoPedidos = () =>
  {
    const {navigate} = this.props.navigation;
    navigate('HistoricoPedidos',{nomeEstab:this.state.nomeEstab})
  }

  goToReverPedidos = () =>{
    const {navigate} = this.props.navigation;
    navigate('ReverPedidos',{nomeEstab:this.state.nomeEstab})
  }

  goToConfigurarImpressora = () =>
  {
    const {navigate} = this.props.navigation;
    navigate('ConfigurarImpressora')
  }

  goToAlterarProduto = () =>
  {
    Alert.alert(
      'Em breve.',
      'Funcionalidade ainda não disponível. Em breve você poderá alterar o valor dos seus produtos direto pelo app.',
      [
        {text: 'OK', onPress: () => {

        }},
      ],
      { cancelable: false }
    )
  }

  async googleLogin() {
    try {
      // add any configuration settings here:
      await GoogleSignin.configure({
        scope:"https://www.googleapis.com/auth/cloudprint",
      webClientId: config.webClientId,
      offlineAccess: false,
    });

      const data = await GoogleSignin.signIn();

      console.log("data googlesignin"+JSON.stringify(data));

      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      // login with credential
      const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);

      console.log(JSON.stringify(firebaseUserCredential.toJSON()));
    } catch (e) {
      console.error(e);
    }
  }



   render() {

     console.ignoredYellowBox = [
         'Setting a timer'
     ]

     const content = this.state.loading ?

     <View style={styles.containerIndicator}>

     </View> :

     <View style={{flex:1}}>
       <Text style={{marginTop: hp('10%'),justifyContent: 'center',alignSelf: 'center',
       fontFamily: 'Futura PT Bold',fontSize: wp('6%'),color:'white'}}>{_.upperCase(this.state.nomeEstab)}</Text>
       <LazyYellowButton
         style={{marginTop:hp('10%')}}
         onPress={()=>{
           this.goToHistoricoPedidos()
         }}
         text={'RELATÓRIOS'}/>
       <LazyYellowButton
         style={{marginTop:hp('6%')}}
         onPress={()=>{
           this.goToReverPedidos()
         }}
         text={'HISTÓRICO DE PEDIDOS'}/>
       <LazyYellowButton
         style={{marginTop:hp('6%')}}
         onPress={()=>{
           this.goToAlterarProduto()
         }}
         text={'ALTERAR PRODUTOS'}/>
       <LazyYellowButton
         style={{marginTop:hp('6%')}}
         onPress={()=>{
           this.goToConfigurarImpressora()
         }}
         text={'CONFIGURAR IMPRESSORAS'}/>
      <LazyYellowButton
        style={{marginTop:hp('6%')}}
        onPress={()=>{
          this.setState({
            nomeEstab: ""
          });
          logout()
          this.goToLogin()
        }}
        text={'LOGOUT 2'}/>
        <LazyYellowButton
          style={{marginTop:hp('6%')}}
          onPress={()=>{
            this.googleLogin()
          }}
          text={'LOGIN COM GOOGLE'}/>


     </View>

     return (
       <ImageBackground
         source={images.backgroundLazy}
         style={styles.backgroundImage}>
         {content}
       </ImageBackground>
     );
     }
}
