
import React, { Component } from 'react';
import {AsyncStorage, Vibration, FlatList, Image, ImageBackground, View, Text,Platform,Picker,PickerIOS, Button, ActivityIndicator, TouchableHighlight, YellowBox, Alert } from 'react-native'
import { styles, cores,images } from '../constants/constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { updateStatusUser,confirmadoRecebimento,preparandoMessages, waitMessages, pedidos, updateStatus, loadStatus,getEstabName } from '../firebase/database'
import PreparandoListItem from './preparandoListItem'
import LazyActivity from '../constants/lazyActivity'
import LazyYellowButton from '../constants/lazyYellowButton'
import {auth} from '../firebase/firebase'
import { NavigationEvents } from 'react-navigation';
import Loader from '../loadingModal/loadingModal';
import _ from 'lodash';
import { NetPrinter } from 'react-native-printer';
import ModalItem from './modalItem'
import { checkInternetConnection } from 'react-native-offline'

let todocount=0
let listener = null
let teste=[];
let user=""
let nomeEstab =""
let tipoEstab=""
const detalhesModal = ""
var i=0

export class PreparandoScreen extends Component {

  static navigationOptions = {
    tabBarLabel: 'EM PREPARO',
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../img/clock.png')}
        style={styles.icon}
        />
    ),
  };

  constructor(props) {
    super(props);
    //Estados para pegar as informações do Usuário
    this.state = {
      loading:false,
      messages:[],
      refreshing: false,
      modalItem:false,
      estabName:"",
      esperandoConfirmacao: false

    }

  }

  showModal(obs){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {obs?<Text style={{marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  closeModal(){
    this.setState({
      modalItem:!this.state.modalItem
    });
  }

  async componentDidMount(){
    this.setState({
      loading:true
    });
    const {state} = this.props.navigation;
    nomeEstab = state.params ? state.params.nomeEstab : ""
    tipoEstab = state.params ? state.params.tipoEstab : ""
    // console.log("state.estabname"+this.state.estabName);
  }

  enviarPedido(item,index){
    const messages = [...this.state.messages]

    ///atualizar status no messages antes de salvar localmente

    Alert.alert(
      'ENVIAR PEDIDO',
      'Deseja confirmar que o pedido está sendo enviado para o cliente?',
      [
        {text: 'Sim', onPress: async () => {
          const isConnected = await checkInternetConnection();

            console.log("isConnected"+isConnected);
          if(isConnected){
            this.setState({
              esperandoConfirmacao:true
            });
          updateStatus(nomeEstab,messages[index]._id,messages[index].idUser,"Pedido Enviado",async (status)=>{
              messages[index].status = "Pedido Enviado"
              messages.splice(index,1)

             await AsyncStorage.setItem('pedidosConfirmadosLocal', JSON.stringify(messages) )
               .then( ()=>{
               console.log("It was saved successfully")
               } )
               .catch( ()=>{
               console.log("There was an error saving the product")
               } )

            this.setState({messages});
            Alert.alert(
              'Pedido concluído com sucesso.',
              'Pedido feito e enviado. Você poderá encontrá-lo no histórico.',
              [
                {text: 'OK', onPress: () => {
                  this.setState({
                    esperandoConfirmacao:false
                  });
                }},
              ],
              { cancelable: false }
            )
          })
        }else{
          this.setState({
            esperandoConfirmacao:false
          },function(){
            alert('Erro de conexão. Tente novamente.')
          });

        }
        }},
        {text: 'Não', onPress: ()=>{
          console.log("cancelado");
        }},
      ],
      {cancelable: false}
    )


  }

  async cancelarPedido(item,index){
    const messages = [...this.state.messages]

    Alert.alert(
      'CANCELAR PEDIDO',
      'Deseja CANCELAR o pedido? Isso indicará que o pedido não foi concluído com sucesso e o cliente não recebeu o produto.',
      [
        {text: 'Sim', onPress: async () => {
          updateStatus(nomeEstab,messages[index]._id,messages[index].idUser,"Pedido Cancelado",async (status)=>{
             messages[index].status = status.status
             messages.splice(index,1)
             await AsyncStorage.setItem('pedidosConfirmadosLocal', JSON.stringify(messages) )
               .then( ()=>{
               console.log("It was saved successfully cancelar pedido")
               } )
               .catch( ()=>{
               console.log("There was an error saving the product")
               } )

              this.setState({messages});
              Alert.alert(
                'Pedido cancelado.',
                'Pedido cancelado. Você poderá encontrá-lo no histórico posteriormente.',
                [
                  {text: 'OK', onPress: () => {

                  }},
                ],
                { cancelable: false }
              )
          })
        }},
        {text: 'Não', onPress: ()=>{
          console.log("cancelado");
        }},
      ],
      {cancelable: false}
    )
  }

  reimprimirPedido=(item,index)=>{
    this.setState({
      loading:true
    }, async function(){
      const impressoras = await AsyncStorage.getItem('impressoras')
      let novaImpressora = JSON.parse(impressoras);
      console.log("impressoras"+JSON.stringify(novaImpressora));
      if( !novaImpressora ){
        Alert.alert(
          'Sem Impressoras.',
          'Não há impressoras cadastradas.',
          [
            {text: 'OK', onPress: () => {
              this.setState({
                loading: false
              });
            }},
          ],
          { cancelable: false }
        )
     }else{
       this.printTextTest(item)
       this.setState({
         loading: false
       });
     }
    });
  }

  printInformacoes=(item)=>{
    if(item.retirar){
      impressaoProdutos = impressaoProdutos.concat("<C>Lazy Delivery - "+nomeEstab+"</C>\n\nNome Cliente: "+
      _.deburr(item.nome)+"\nTelefone: "+item.telefone+"\n<C>Pedido ira ser retirado no estabelecimento.</C>\n\n<C>------------------------------------------</C>\nQtde Item                         Preco\n<C>------------------------------------------</C>\n");
    }else{
      impressaoProdutos = impressaoProdutos.concat("<C>Lazy Delivery - "+nomeEstab+"</C>\n\nNome Cliente: "+_.deburr(item.nome)+
      "\nTelefone: "+item.telefone+"\nEndereco: "+_.deburr(item.endereco)+"\nBairro: "+_.deburr(item.bairro)+
      "\nReferencia: "+_.deburr(item.referencia)+"\n\n<C>------------------------------------------</C>\nQtde Item                         Preco\n<C>------------------------------------------</C>\n");
    }
  }

  printProdutos=(item)=>{

    item.carrinho.map((item1,index)=>{
      let str = (item1.preco*item1.quantidade).toFixed(2)
      let res = str.toString().replace(".",",")
      var length = item1.nome.length
      let blankSpace=" "
      var espacos=0
      var adicional = ""
      var tipoProd = ""
      if(length>30){
        //duas linhas
        espacos = 49-(length-30)
      }else if(length<=30){
        //uma linha
        espacos = 29 - length
      }
      if(item1.adicional==true){
        espacos = espacos - 11
        adicional = "ADICIONAL: "
        tipoProd = ""
      }else{
        tipoProd = item1.tipoProduto
      }

      if(item1.tipoProduto=="Pizzas"){
        //se for pizza imprimir os sabores da pizza
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n"+_.deburr(item1.detalhes)+" \n")
        if(item1.obs){
          //pizza com alguma observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\nOBS: "+_.deburr(item1.obs)+" \n");
        }
      }else if(item1.obs){
        //item com observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n     "+_.deburr(tipoProd)+"\nOBS: "+_.deburr(item1.obs)+"\n");
      }else{
        //item sem observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+adicional+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n"+"     "+_.deburr(tipoProd)+"\n");
      }
    })
    // NetPrinter.printText(impressaoProdutos)
  }

  printValorHorario=(item)=>{

    var date = new Date(item.createdAt)
    var year = date.getFullYear();
    var month = date.getMonth() + 1; // getMonth() is zero-indexed, so we'll increment to get the correct month number
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;
    hours = (hours < 10) ? '0' + (hours) : (hours);
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds: seconds;

    console.log("year"+year);
    console.log("month"+month);
    console.log("day"+day);
    console.log("hours"+hours);
    console.log("minutes"+minutes);
    console.log("seconds"+seconds);


    let totalProdStr = parseFloat(item.total)
    totalProdStr = totalProdStr.toFixed(2)
    let totalProd = totalProdStr.toString().replace(".",",")

    let taxaEntregaStr = parseFloat(item.frete)
    taxaEntregaStr = taxaEntregaStr.toFixed(2)
    let taxaEntrega = taxaEntregaStr.toString().replace(".",",")

    let totalStr = parseFloat(item.total+item.frete)
    totalStr = totalStr.toFixed(2)
    let total = totalStr.toString().replace(".",",")

    if(item.formaPgto=="Dinheiro"){
        impressaoProdutos = impressaoProdutos.concat("<C>---------------------------------------</C>\n<C>Produtos : R$"+totalProd+
        "</C>\n<C>Taxa de Entrega: R$"+taxaEntrega+
        "</C>\n<C>---------------------------------------</C>\n<CD>Valor Total: R$"+total+
        "</CD>\n<C>---------------------------------------</C>\nForma Pgto: "+_.deburr(item.formaPgto)+
        " - Troco para: "+item.formaPgtoDetalhe+"\nData Pedido: "+day+"/"+month+"/"+year+"\nHorario Pedido: "+hours+":"+minutes+":"+seconds+"");
    }else{
        impressaoProdutos = impressaoProdutos.concat("<C>------------------------------------------</C>\n<C>Produtos : R$"+totalProd+
        "</C>\n<C>Taxa de Entrega: R$"+taxaEntrega+
        "</C>\n<C>------------------------------------------</C>\n<CD>Valor Total: R$"+total+
        "</CD>\n<C>------------------------------------------</C>\nForma Pgto: "+_.deburr(item.formaPgto)+
        " - "+item.formaPgtoDetalhe+"\nData Pedido: "+day+"/"+month+"/"+year+"\nHorario Pedido: "+hours+":"+minutes+":"+seconds+"");
    }
  }

  printTextTest = (item) => {
      impressaoProdutos=""
        this.printInformacoes(item)
        this.printProdutos(item)
        this.printValorHorario(item)
        NetPrinter.printBill(impressaoProdutos)
  }



  // arquivarPedido=(item,index)=>{
  //   const messages = [...this.state.messages]
  //   messages.splice(index,1)
  //   this.setState({messages});
  // }

  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };

  preparandoMessagesFunction(){
    this.setState({
      loading:true
    },async function(){
      const pedidosConfirmados = await AsyncStorage.getItem('pedidosConfirmadosLocal')
      let novosPedidos = JSON.parse(pedidosConfirmados);
      console.log("novosPedidos preparandoMessagesFunction"+JSON.stringify(novosPedidos));
      if( !novosPedidos ){
        novosPedidos = []
      }

      this.setState({
        messages: novosPedidos,
        loading: false
      },function(){

      });

    });


  }

  showModal(obs){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {obs?<Text style={{fontFamily: 'Futura Book',marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  showModalPizza(obs,detalhes){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {detalhes?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>{detalhes}</Text>:<View></View>}
        {obs?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }


  functionListaPedidos(){
    if(this.state.messages){
    return(
      <FlatList
        refreshing={this.state.refreshing}
        ItemSeparatorComponent={this.renderSeparatorComponent}
        data={this.state.messages}
        extraData={this.state}
        renderItem={({item,index})=>
        <PreparandoListItem
          enviarPedido={()=>this.enviarPedido(item,index)}
          cancelarPedido={()=>this.cancelarPedido(item,index)}
          arquivarPedido={()=>this.arquivarPedido(item,index)}
          reimprimirPedido={()=>this.reimprimirPedido(item,index)}
          item={item}
          detalhesObs={(obs)=>{
            this.showModal(obs)
          }}
          detalhesPizzas={(obs,detalhes)=>{
            this.showModalPizza(obs,detalhes)
          }}
          >
        </PreparandoListItem>}
        keyExtractor={item => item._id}
        />
    )}else{
      return(
      <View>
        <Text>Sem pedidos no histórico.</Text>
      </View>)
    }
  }

  render() {

    console.ignoredYellowBox = [
      'Setting a timer'
    ]

    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <LazyActivity/>
    </View> :

    <View style={{flex:1}}>

      {this.functionListaPedidos()}
    </View>


    return (
      <ImageBackground
        source={images.imageBackground}
        style={styles.backgroundImage}>
        <Loader
            loading={this.state.esperandoConfirmacao}
            message="O status está sendo atualizado." />
        <NavigationEvents
          onDidFocus={()=>{
            this.preparandoMessagesFunction()}}/>
          <ModalItem
            detalhes={this.detalhesModal}
            loading = {this.state.modalItem}
            showModal = {()=>{this.closeModal()}}/>
          <Text style={{alignSelf: 'center',fontSize: wp('4%'),
              marginBottom: 10, color:cores.corPrincipal,fontFamily: 'Futura Medium'}}>Pedidos sendo preparados ({this.state.messages.length})</Text>
          {content}
      </ImageBackground>
    );
  }

}
//

// <LazyYellowButton style={{alignContent: 'flex-end',marginBottom: hp('2%')}}
//   onPress={()=>this.openEstabelecimento()}
//   text={this.state.aberto?'FECHAR':'ABRIR'}/>
