import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Button,FlatList,ScrollView } from 'react-native';
import { styles, cores } from '../constants/constants'
import CarrinhoListItem from './carrinhoListItem'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class HomeListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status:false
    }
  }

  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };

  valorVirgula(valor){
    var str = parseFloat(valor)
    str = str.toFixed(2)
    var res = str.toString().replace(".",",")
    return(
        <Text>{res}</Text>
    )
  }


  preco=(item)=>{
    {
      var str = (item.preco*item.quantidade).toFixed(2)
      var res = str.toString().replace(".",",")
      let fontSize=0
      if(item.adicional==true){
        fontSize=wp('3.5%')
      }else{
        fontSize=wp('3.75%')
      }
      return(
          <Text style={[styles.textCarrinho,{
              color: cores.textDetalhes,
              fontFamily: "Futura Medium Italic BT",
              alignSelf: 'center', fontSize: fontSize}]}>
            R$ {res}
          </Text>
      )
    }
  }

  functionNome(item){
    if(item.tipoProduto=="Pizzas"){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesPizzas(item.obs,item.detalhes)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
      )
    }else if(item.detalhes){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesPizzas(item.obs,item.detalhes)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
      )
    }else if(item.obs){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesObs(item.obs)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
    )}else{
      return(
        <Text style={[styles.textCarrinho,
            {alignSelf: 'flex-start',
            marginHorizontal: 10,
          fontSize: wp('3.5%')}]}>
        {item.nome} - {item.tipoProduto}
        </Text>
      )
  }
  }


  functionCarrinhoListItem=(item)=>{
    // console.log("new date"+new Date());
    console.log("functionCarrinhoListItEM");
    var year = item.createdAt.getUTCFullYear();
    var month = item.createdAt.getUTCMonth() + 1; // getMonth() is zero-indexed, so we'll increment to get the correct month number
    var day = item.createdAt.getUTCDate();
    console.log("day"+day);
    var hours = item.createdAt.getUTCHours();
      console.log("hours"+parseFloat(hours));
    var minutes = item.createdAt.getUTCMinutes();
      console.log("minutes"+minutes);
    var seconds = item.createdAt.getUTCSeconds();
      console.log("seconds"+seconds);
    var timeZone = new Date(item.createdAt).getTimezoneOffset()

    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;
    console.log("hours: "+hours);
    hours = (hours == 0) ? 24 : hours
    hours = (hours < 10) ? '0' + hours-3 : hours-3;
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds: seconds;
    // <Text style={[styles.textCarrinho]}>
    //   Horário do Pedido: {hours+":"+minutes+":"+seconds}
    // </Text>
    // <Text style={[styles.textCarrinho]}>
    //   Data do Pedido: {day+"/"+month+"/"+year}
    // </Text>
    console.log("after day"+day);
    console.log("after hours"+hours);
    console.log("after minutes"+minutes);
    console.log("after seconds"+seconds);
      return (
          <View
            style={{borderWidth: 2, marginHorizontal: 2,borderColor: cores.corPrincipal}}>
            <View style={{flexDirection: 'column', justifyContent: 'flex-start'}}>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.25%'),color: cores.textDetalhes}]}>Nome do Cliente: </Text>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.25%'),}]}>{item.nome}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Telefone: </Text>
                  <Text style={[styles.textCarrinho]}>{item.telefone}</Text>
                </View>
                {item.retirar?
                  <Text style={[styles.textCarrinho,{marginLeft: 5}]}>Pedido irá ser retirado no estabelecimento.</Text> :
                  <View>
                  <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                    <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Endereco: </Text>
                    <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.endereco}</Text>
                  </View>
                  <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                    <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Bairro: </Text>
                    <Text style={[styles.textCarrinho]}>{item.bairro}</Text>
                  </View>
                  <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                    <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Referência: </Text>
                    <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.referencia}</Text>
                  </View>
                  </View>
                   }

                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Pedido:</Text>
                </View>
                {item.carrinho.map((item,index)=>{
                  if(item.adicional==false){
                    console.log("dentroitem.adiciona==false");
                    return (
                      <View style={{flex:1,
                          height:hp('6%'), flexDirection: 'row',justifyContent: 'space-between',
                          alignItems: 'center',marginHorizontal: wp('1%')}}
                          key={index}>

                        <View style={
                            {
                            borderColor: cores.corSecundaria,
                            borderWidth: 1,
                            height:hp('6%'),
                            justifyContent: 'center',
                            width:wp('18%')}}>
                            <Text style={[styles.textCarrinho,
                                {alignSelf: 'center',
                                marginLeft: 5,
                              fontSize: wp('3.75%')}]}>{item.quantidade}x
                            </Text>
                          </View>

                          <View style={{width:wp('54%'),height:hp('6%'), justifyContent: 'center',
                            borderColor: cores.corSecundaria,borderWidth: 1}}>
                            {this.functionNome(item)}
                          </View>

                          <View style={{width: wp('24%'),height:hp('6%'),marginRight: 10,
                            justifyContent: 'center',borderColor: cores.corSecundaria,borderWidth: 1}}>
                            {this.preco(item)}
                          </View>

                      </View>
                    );
                  }else{
                    return (
                      <View style={{flex:1,
                          height:hp('6%'), flexDirection: 'row',marginHorizontal: wp('1%'),justifyContent: 'space-between',
                          alignItems: 'center'}}
                          key={index}>

                        <View style={
                            {backgroundColor:'rgba(252, 204, 60,0.5)',
                            borderColor: cores.corSecundaria,
                            borderWidth: 1,
                            height:hp('6%'),
                            justifyContent: 'center',
                            width:wp('18%')}}>
                            <Text style={[styles.textCarrinho,
                                {alignSelf: 'center',
                                marginLeft: 5,
                              fontSize: wp('3.75%')}]}>{item.quantidade}
                            </Text>
                          </View>

                          <View style={{backgroundColor:'rgba(252, 204, 60,0.5)',width:wp('54%'),height:hp('6%'), justifyContent: 'center',
                            borderColor: cores.corSecundaria,borderWidth: 1}}>
                              <Text style={[styles.textCarrinho,
                                  {alignSelf: 'flex-start',
                                  marginHorizontal: 10,
                                fontSize: wp('3%')}]}>
                              {item.nome}
                              </Text>
                          </View>

                          <View style={{backgroundColor:'rgba(252, 204, 60,0.5)',width: wp('24%'),height:hp('6%'),marginRight: 10,
                            justifyContent: 'center',borderColor: cores.corSecundaria,borderWidth: 1}}>
                            {this.preco(item)}
                          </View>

                      </View>
                    );
                  }
                })}
                <View style={{flexDirection: 'row',marginLeft: wp('2%'),marginTop: hp('1%')}}>
                  <Text style={[styles.textCarrinho,{color: 'red',fontSize: wp('3.25%')}]}>IMPORTANTE: Itens sublinhados contém observações em relação ao item. Clique para verificar. </Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%'),marginTop: hp('1%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Total: </Text>
                  <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.total)}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Taxa de Entrega: </Text>
                  {item.retirar?
                    <Text style={[styles.textCarrinho]}>Pedido irá ser retirado no estabelecimento.</Text> :
                    <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.frete)}</Text>  }
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Total com taxa: </Text>
                  <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.total+item.frete)}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Forma de Pagamento: </Text>
                  {item.formaPgto=="Dinheiro" ?
                    <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.formaPgto} - Troco para: R${item.formaPgtoDetalhe}</Text> :
                    <Text style={[styles.textCarrinho]}>{item.formaPgto} - {item.formaPgtoDetalhe}</Text>
                  }
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),color: cores.textDetalhes}]}>Data do Pedido: </Text>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),}]}>{day+"/"+month+"/"+year}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),color: cores.textDetalhes}]}>Horário do Pedido: </Text>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),}]}>{hours+":"+minutes}</Text>
                </View>
                {item.status=="Aguardando Confirmação"?
                  <View style={{flexDirection: 'column'}}>
                    <TouchableOpacity
                      style={{height: hp('6.5%'),alignItems: 'center',
                        justifyContent: 'center',backgroundColor: 'green',flex:1,
                        borderColor: cores.corPrincipal,borderWidth: 0.5,borderBottomWidth: 8}}
                      onPress={this.props.confirmarPedido}>
                      <Text style={{color:cores.corPrincipal,fontSize: wp('3.5%'),fontFamily: 'Futura Medium',color: cores.corSecundaria}}>Confirmar Recebimento</Text>
                    </TouchableOpacity>
                  </View>
                  :
                  <View></View>
                }
                <Text style={[item.status=="Confirmado Recebimento" ? styles.recebidoStatus : styles.aguardandoStatus,
                  {fontSize: 16  , alignSelf: 'center',fontFamily: 'Futura Medium'}]}>Status do Pedido: {item.status}</Text>
            </View>
          </View>
      );
  }

  render() {
    const { item } = this.props;
    return(
      <View>
        {this.functionCarrinhoListItem(item)}
      </View>
    )
  }
}
// <FlatList
//   ItemSeparatorComponent={this.renderSeparatorComponent}
//   data={item.carrinho}
//   extraData={this.state}
//   renderItem= {
//     ({item}) =>
//     <CarrinhoListItem
//       item ={item}
//       preco={() => {
//         var str = (item.preco*item.quantidade).toFixed(2)
//         var res = str.toString().replace(".",",")
//         let fontSize=0
//         if(item.adicional==true){
//           fontSize=13
//         }else{
//           fontSize=15
//         }
//         return(
//             <Text style={[styles.textCarrinho,{
//                 color: cores.textDetalhes,
//                 fontFamily: "Futura Medium Italic BT",
//                 alignSelf: 'center', fontSize: fontSize}]}>
//               R$ {res}
//             </Text>
//         )
//       }}>
//     </CarrinhoListItem>}
//   keyExtractor={item => item.id}
//   />
// <View style={{flexDirection: 'row'}}>
//   <TouchableOpacity
//     style={{width: wp('40%'),height: hp('5.5%'),alignItems: 'center',
//       justifyContent: 'center',backgroundColor: 'red',flex:1,borderColor: cores.corPrincipal,borderWidth: 0.5,borderBottomWidth: 1}}
//     onPress={this.props.estabFechado}>
//     <Text style={{color:cores.corPrincipal,fontSize: wp('3%'),fontFamily: 'Futura Medium'}}>Estabelecimento Fechado</Text>
//   </TouchableOpacity>
//   <TouchableOpacity
//     style={{width: wp('40%'),height: hp('5.5%'),alignItems: 'center',
//       justifyContent: 'center',backgroundColor: 'red',flex:1,borderColor: cores.corPrincipal,borderWidth: 0.5,borderBottomWidth: 1}}
//     onPress={this.props.semItem}>
//     <Text style={{color:cores.corPrincipal,fontSize: wp('3%'),fontFamily: 'Futura Medium'}}>Sem disponibilidade de item</Text>
//   </TouchableOpacity>
// </View>
