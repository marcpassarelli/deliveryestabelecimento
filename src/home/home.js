
import React, { Component } from 'react';
import { AppState, TouchableOpacity,AsyncStorage, StyleSheet,Modal, Platform, Vibration,TextInput, FlatList, Image, ImageBackground, View, Text, Button, ActivityIndicator, TouchableHighlight, YellowBox, Alert,ScrollView } from 'react-native'
import { styles, cores,images } from '../constants/constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { setToken, getTokenDB, checkUpdateStatusUser,checkPedidoUser, setDate,salvarPedidoOcioso, confirmadoRecebimento, preparandoMessages,updateStatusUser, updateStatusSemItem, removeMessage,loadMessages, waitMessages, pedidos, updateStatus, loadStatus,getEstabName } from '../firebase/database'
import HomeListItem from './homeListItem'
import LazyActivity from '../constants/lazyActivity'
import LazyYellowButton from '../constants/lazyYellowButton'
import {auth} from '../firebase/firebase'
import { checkInternetConnection } from 'react-native-offline';
import NotifConfig from './notifConfig'
import firebase from 'react-native-firebase';
import { Notification, NotificationOpen } from 'react-native-firebase';
import ModalItem from './modalItem'
import _ from 'lodash';
import { NetPrinter, USBPrinter } from 'react-native-printer';
import Loader from '../loadingModal/loadingModal';
import RNPrint from 'react-native-print';

// import firebaseClient from  "../firebase/firebaseClient";

let todocount=0
let listener = null
let user=""
let nomeEstab =""
let tipoEstab=""
const detalhesModal = ""
var impressaoProdutos = ""

export class HomeScreen extends Component {

  static navigationOptions = {
    tabBarLabel: 'PENDENTES',
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../img/delivery.png')}
        style={[styles.icon]}
        />
    ),
  };

  constructor(props) {
    super(props);
    //Estados para pegar as informações do Usuário
    this.state = {
      loading:false,
      messages:[],
      refreshing: false,
      estabName:"",
      modalSemItem:false,
      modalItem:false,
      itemIndisponivel:[],
      esperandoConfirmacao: false,
      appState: AppState.currentState,
    }
    this.notif = new NotifConfig(this.onRegister.bind(this), this.onNotif.bind(this));

  }

  onRegister(token) {
    Alert.alert("Registered !", JSON.stringify(token));
    console.log(token);
    this.setState({ registerToken: token.token, gcmRegistered: true });
  }

  onNotif(notif) {
    console.log(notif);
    Alert.alert(notif.title, notif.message);
  }

  showModal(obs){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {obs?<Text style={{fontFamily: 'Futura Book',marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  showModalPizza(obs,detalhes){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {detalhes?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>{detalhes}</Text>:<View></View>}
        {obs?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  closeModal(){
    this.setState({
      modalItem:!this.state.modalItem
    });
  }


    _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({appState: nextAppState},function(){
      console.log("appState: "+this.state.appState);
    });
  }

  componentWillUnmount(){
    this.removeNotificationDisplayedListener();
    this.removeNotificationListener();
    this.removeNotificationOpenedListener()
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  async componentDidMount(){
    AppState.addEventListener('change', this._handleAppStateChange);
    console.log("dentro didmount home");
    //Configura NOtificação
   //  USBPrinter.getDeviceList()
   // .then(printers => {
   //   this.setState(Object.assign({}, this.state, {printers: printers}))
   // })
   // console.log("printers"+JSON.stringify(this.state.printers));
    // this.notif.configure(this.onRegister.bind(this), this.onNotif.bind(this))

    this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {

        console.log("notification1"+notification.notifcation);
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        // const notification: Notification = notificationOpen.notification;
    });
    this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {

      var keys = Object.keys(notification);
      console.log("notification._body"+notification._body);
      console.log("notification._data"+notification._data);
      console.log("notification._title"+notification._title);
      console.log("notification._subtitle"+notification._subtitle);
      // console.log("notification2 data"+JSON.stringify(notification2));
        // Process your notification as required
    });
    this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        console.log("action "+action);
        // Get information about the notification that was opened
        const notificationOpened: Notification = notificationOpen.notification;
        console.log("notification3"+JSON.stringify(notificationOpened));
    });

    this.setState({
      loading
      :true
    });

    this.checkPermission();

    //pega quantidade de impressões por pedido
    var qtdeImpressoes = await AsyncStorage.getItem('qtdeImpressoes')
    if(!qtdeImpressoes){
      qtdeImpressoes = 0
    }else{
      this.setState({
        qtdeImpressoes:qtdeImpressoes
      },function(){
        console.log("qtdeImpressoes"+this.state.qtdeImpressoes);
      });
    }

    //configurar impressoras instaladas
    if(Platform.OS == 'android'){

      const impressoras = await AsyncStorage.getItem('impressoras')
      let novaImpressora = JSON.parse(impressoras);
      console.log("impressoras"+JSON.stringify(novaImpressora));
      if( !novaImpressora ){
        console.log("nenhuma impressora");
        novaImpressora = []
     }else{
       novaImpressora.map((printer) =>{
         let ip = String(printer.ip)
         let porta = parseInt(printer.porta)
           NetPrinter.init().then(() => {
              // this.setState(Object.assign({}, this.state, {printers: [{host: '192.168.1.107', port: 9100}]}))
              this._connectPrinter(ip, porta)
           })
       })

     }
    }

    const {state} = this.props.navigation;
    nomeEstab = state.params ? state.params.nomeEstab : ""
    tipoEstab = state.params ? state.params.tipoEstab : ""


    preparandoMessages(nomeEstab,async ()=>{
      var messagesOrganized=_.orderBy(confirmadoRecebimento,['createdAt'],['desc'])

         await AsyncStorage.setItem('pedidosConfirmadosLocal', JSON.stringify(messagesOrganized) )
           .then( ()=>{
           console.log("It was saved successfully")
           } )
           .catch( ()=>{
           console.log("There was an error saving the product")
           } )
        // atualizaPedidosConfirmados(this.state.messages)
        // console.log("state.messages"+JSON.stringify(this.state.messages));

    })


    waitMessages(nomeEstab,(message)=>{
      this.setState({
        loading:true
      });

      const messages = [...this.state.messages]
      if(message.nome){
        if(message.new==true){
          console.log("novo");
          // RNLocalNotifications.createNotification(1, 'Some text', '2017-01-02 12:30', 'default');
          if(this.state.appState!="background"){
            this.notif.localNotif()
          }
          // Vibration.vibrate()
        }

        let frete=0;
        if(message.retirar==true){
          frete=0
        }else if(message.retirar==false){
          frete = message.frete
        }

        messages.push({
          _id:message._id,
          idUser: message.idUser,
          estabelecimento:nomeEstab,
          token:message.token,
          nome:message.nome,
          telefone:message.telefone,
          endereco:message.endereco,
          bairro: message.bairro,
          referencia: message.referencia,
          formaPgto: message.formaPgto,
          formaPgtoDetalhe: message.formaPgtoDetalhe,
          retirar:message.retirar,
          carrinho: message.carrinho,
          createdAt: message.createdAt,
          status: message.status,
          frete:frete,
          total:message.total
        })
        console.log("messagesOrganized"+messages);
        var messagesOrganized=_.orderBy(messages,['createdAt'],['desc'])
        this.setState({messages:messagesOrganized},function(){
          //quantidade de impressões e impressão
          var times = this.state.qtdeImpressoes;
          for(var i=0; i < times; i++){
            this.printTextTest(message)
          }
          console.log("messages.state"+this.state.messages);
          this.setState({loading:false})
        })

      }
      else{
        this.setState({loading:false})
      }

    })

    this.setState({loading:false})

  }

  async printHTML() {
   await RNPrint.print({
     html: '<h1>Heading 1</h1><h2>Heading 2</h2><h3>Heading 3</h3>'
   })
 }

  async checkPermission() {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    console.log("checkPermission"+enabled);
      this.getToken();
  } else {
      this.requestPermission();
  }
  }

  //3
  async getToken() {
    let user = await auth.currentUser
    getTokenDB(nomeEstab,
      async (c)=>{
        fcmToken = await firebase.messaging().getToken();
        console.log("fcmToken: "+fcmToken);
        console.log("c.token: "+JSON.stringify(c.token));
        if(c.token != fcmToken){
          setToken(nomeEstab,fcmToken)
        }
      })
  }

  //2
  async requestPermission() {
  try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
  } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
  }
  }

  async confirmarPedido(item,index){

    const messages = [...this.state.messages]

    const isConnected = await checkInternetConnection();

    console.log("isConnected"+isConnected);
    if(isConnected){
      this.setState({
        esperandoConfirmacao:true
      },function(){
        setDate(nomeEstab,messages[index].createdAt,messages[index]._id)
      });
    //updateStatus no firebase
    console.log("messages.id"+messages[index]._id);
    updateStatus(nomeEstab,messages[index]._id,messages[index].idUser,"Confirmado Recebimento",async (status)=>{
      //
      // if(messages[index].idUser=="semCadastro"){
        const pedidosConfirmados = await AsyncStorage.getItem('pedidosConfirmadosLocal')
        let novosPedidos = JSON.parse(pedidosConfirmados);

        if( !novosPedidos ){
          novosPedidos = []
        }
        ///atualizar status no messages antes de salvar localmente
        messages[index].status="Confirmado Recebimento"
        console.log("messages depois confirmado Recebimento"+JSON.stringify(messages[index]));
        novosPedidos.push(messages[index])
        await AsyncStorage.setItem('pedidosConfirmadosLocal', JSON.stringify(novosPedidos) )
          .then( ()=>{
          //   console.log("novosPedidos confirmarPedido"+JSON.stringify(novosPedidos));
          // console.log("It was saved successfully")
          } )
          .catch( ()=>{
          console.log("There was an error saving the product")
          } )

      messages.splice(index,1)
      this.setState({messages},function(){

        //quantidade de impressões e impressão
        var times = this.state.qtdeImpressoes;
        for(var i=0; i < times; i++){
          this.printHTML()
          this.printTextTest(item)
        }
        this.setState({
          esperandoConfirmacao:false
        });
      });

    })
    }else{
      this.setState({
        esperandoConfirmacao:false
      });
      alert("Erro de conexão. O pedido não foi atualizado corretamente. Tente de novo.")
    }
  }

  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };

  functionListaPedidos(){
    // console.log("message function listapedidos"+this.state.messages);
  if(Object.keys(this.state.messages).length>0){

    return(
      <FlatList
        refreshing={this.state.refreshing}
        ItemSeparatorComponent={this.renderSeparatorComponent}
        data={this.state.messages}
        extraData={this.state}
        renderItem={({item,index})=>
        <HomeListItem
          confirmarPedido={()=>this.confirmarPedido(item,index)}

          item={item}
          detalhesObs={(obs)=>{
            this.showModal(obs)
          }}
          detalhesPizzas={(obs,detalhes)=>{
            this.showModalPizza(obs,detalhes)
          }}>
        </HomeListItem>}
        keyExtractor={item => item._id}
        />
    )}else{
      console.log("dentro else functionListaPedidos");
        return(<Text style={styles.textAddProduto}>Sem pedidos pendentes.</Text>)
    }
  }

  _connectPrinter = (host, port) => {
    this.setState({
      loading:true
    });
    if(Platform.OS == 'android'){
      //connect printer
      NetPrinter.connectPrinter(host, port).then(
        (printer) => {
          this.setState(Object.assign({}, this.state, {currentPrinter: printer}))
          this.setState({
            loading:false,
          });
        },
        error => {
          this.setState({
            loading: false
          },function(){
            Alert.alert(
              'Erro Conexão Impressora',
              error+' Ocorreu um erro ao tentar conectar a impressora de IP: '+host+". Exclua esse ip na página Configurar Impressoras em Meu Pefil.",
               [
                 {text: 'OK', onPress: () => {
                   this.setState({
                     esperandoConfirmacao:false
                   });
                   const { navigate } = this.props.navigation;
                   navigate('Carrinho')
                 }},
               ],
               { cancelable: false }
            )
          });
        })
    }
  }

  printInformacoes=(item)=>{
    if(item.retirar){
      impressaoProdutos = impressaoProdutos.concat("<C>Lazy Delivery - "+nomeEstab+"</C>\n\nNome Cliente: "+
      _.deburr(item.nome)+"\nTelefone: "+item.telefone+"\n<C>Pedido ira ser retirado no estabelecimento.</C>\n\n<C>------------------------------------------</C>\nQtde Item                         Preco\n<C>------------------------------------------</C>\n");
    }else{
      impressaoProdutos = impressaoProdutos.concat("<C>Lazy Delivery - "+nomeEstab+"</C>\n\nNome Cliente: "+_.deburr(item.nome)+
      "\nTelefone: "+item.telefone+"\nEndereco: "+_.deburr(item.endereco)+"\nBairro: "+_.deburr(item.bairro)+
      "\nReferencia: "+_.deburr(item.referencia)+"\n\n<C>------------------------------------------</C>\nQtde Item                         Preco\n<C>------------------------------------------</C>\n");
    }
  }

  printProdutos=(item)=>{
    console.log("item.carrinho"+JSON.stringify(item.carrinho));

    item.carrinho.map((item1,index)=>{
      let str = (item1.preco*item1.quantidade).toFixed(2)
      let res = str.toString().replace(".",",")
      var length = item1.nome.length
      let blankSpace=" "
      var espacos=0
      var adicional = ""
      var tipoProd = ""
      if(length>30){
        //duas linhas
        espacos = 49-(length-30)
      }else if(length<=30){
        //uma linha
        espacos = 29 - length
      }
      if(item1.adicional==true){
        espacos = espacos - 11
        adicional = "ADICIONAL: "
        tipoProd = ""
      }else{
        tipoProd = item1.tipoProduto
      }

      if(item1.tipoProduto=="Pizzas"){
        //se for pizza imprimir os sabores da pizza
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n"+_.deburr(item1.detalhes)+" \n")
        if(item1.obs){
          //pizza com alguma observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\nOBS: "+_.deburr(item1.obs)+" \n");
        }
      }else if(item1.obs){
        //item com observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n     "+_.deburr(tipoProd)+"\nOBS: "+_.deburr(item1.obs)+"\n");
      }else{
        //item sem observação
        impressaoProdutos = impressaoProdutos.concat("  "+item1.quantidade+"  "+adicional+_.deburr(item1.nome)+blankSpace.repeat(espacos)+"R$"+res+"\n"+"     "+_.deburr(tipoProd)+"\n");
      }
    })
    // NetPrinter.printText(impressaoProdutos)
  }

  printValorHorario=(item)=>{
    var year = item.createdAt.getUTCFullYear();
    var month = item.createdAt.getUTCMonth() + 1; // getMonth() is zero-indexed, so we'll increment to get the correct month number
    var day = item.createdAt.getUTCDate();
    var hours = item.createdAt.getUTCHours();
    var minutes = item.createdAt.getUTCMinutes();
    var seconds = item.createdAt.getUTCSeconds();

    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;
    hours = (hours == 0) ? 24 : hours
    hours = (hours < 10) ? '0' + (hours-3) : (hours-3);
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds: seconds;

    let totalProdStr = parseFloat(item.total)
    totalProdStr = totalProdStr.toFixed(2)
    let totalProd = totalProdStr.toString().replace(".",",")

    let taxaEntregaStr = parseFloat(item.frete)
    taxaEntregaStr = taxaEntregaStr.toFixed(2)
    let taxaEntrega = taxaEntregaStr.toString().replace(".",",")

    let totalStr = parseFloat(item.total+item.frete)
    totalStr = totalStr.toFixed(2)
    let total = totalStr.toString().replace(".",",")

    if(item.formaPgto=="Dinheiro"){
        impressaoProdutos = impressaoProdutos.concat("<C>---------------------------------------</C>\n<C>Produtos : R$"+totalProd+
        "</C>\n<C>Taxa de Entrega: R$"+taxaEntrega+
        "</C>\n<C>---------------------------------------</C>\n<CD>Valor Total: R$"+total+
        "</CD>\n<C>---------------------------------------</C>\nForma Pgto: "+_.deburr(item.formaPgto)+
        " - Troco para: "+item.formaPgtoDetalhe+"\nData Pedido: "+day+"/"+month+"/"+year+"\nHorario Pedido: "+hours+":"+minutes+":"+seconds+"");
    }else{
        impressaoProdutos = impressaoProdutos.concat("<C>------------------------------------------</C>\n<C>Produtos : R$"+totalProd+
        "</C>\n<C>Taxa de Entrega: R$"+taxaEntrega+
        "</C>\n<C>------------------------------------------</C>\n<CD>Valor Total: R$"+total+
        "</CD>\n<C>------------------------------------------</C>\nForma Pgto: "+_.deburr(item.formaPgto)+
        " - "+item.formaPgtoDetalhe+"\nData Pedido: "+day+"/"+month+"/"+year+"\nHorario Pedido: "+hours+":"+minutes+":"+seconds+"");
    }
  }

  printTextTest = (item) => {
    if(this.state.currentPrinter) {
      impressaoProdutos=""
        this.printInformacoes(item)
        this.printProdutos(item)
        this.printValorHorario(item)
        NetPrinter.printBill(impressaoProdutos)
    }else{
      console.log("não imprimiu texto")
    }
}



  render() {

    console.ignoredYellowBox = [
      'Setting a timer'
    ]

    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <LazyActivity/>
    </View> :

    <View style={{flex:1}}>
      <Text style={{alignSelf: 'center',fontSize: wp('4%'),
        marginBottom: 10, color:cores.corPrincipal,fontFamily: 'Futura Medium'}}>Pedidos Pendentes</Text>
      {this.functionListaPedidos()}
    </View>

    return (
      <ImageBackground
        source={images.imageBackground}
        style={styles.backgroundImage}>
        <Loader
          loading={this.state.esperandoConfirmacao}
          message="O status está sendo atualizado." />
        <ModalItem
          detalhes={this.detalhesModal}
          loading = {this.state.modalItem}
          showModal = {()=>{this.closeModal()}}/>
        {content}
      </ImageBackground>
    );
  }
}
  // {this.modalSemItemFunction()}

const stylesLocal = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(0,131,139,0.8)'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: hp('50%'),
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});
