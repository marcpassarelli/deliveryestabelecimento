console.ignoredYellowBox = [
    'Setting a timer'
]
import { StyleSheet, Alert, Navigator } from 'react-native';
import React, { Component } from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const cores = {
    corPrincipal: '#472c82',
    corSecundaria: 'rgb(252, 204, 60)',
    textDetalhes:'rgb(43, 189, 204)',
};

export const images ={
  imageBackground: require('../../img/transparent.png'),
  backgroundSplash: require('../../img/01-Front---background.png'),
  iconSplash: require('../../img/Icon---Roxo.png'),
  iconYellow: require('../../img/Logo-amarelo.png'),
  backgroundLazy: require('../../img/cadastro-01-bg.png'),
  backgroundLogin: require('../../img/Login-background.png'),
  backgroundLoginEmail: require('../../img/Login-02-background.png'),
  logoLogin: require('../../img/Logo-White.png'),
  seta2: require('../../img/Seta-02.png'),
  preguicaRating:require('../../img/Preguica-contorno.png')
}

export const styles = StyleSheet.create({

  aguardandoStatusContainer:{
    borderColor: cores.corPrincipal,
  },
  recebidoStatusContainer:{
    borderColor: '#009933',
  },
  aguardandoStatus:{
    color: cores.corPrincipal,
  },
  recebidoStatus:{
    color: '#009933',
  },
  activityIndicator:{
    justifyContent:'center',
    alignItems:'center',
    height:70
  },
  backgroundImage: {
    flex:1,
    height: null,
    width: null
  },
  buttonFacebook: {
    height: 60,
    width: null,
    resizeMode: 'contain',
    borderRadius: 6,
  },
  buttons:{
    height: hp('5.94%'),
    width: wp('75.28%'),
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(252, 204, 60)',
    borderRadius: 10
  },
  containerBotaoRecebimento:{
    width: wp('40%'),
    height: hp('4%'),
    alignItems: 'center',
    justifyContent: 'center',
    flex:1,
    borderColor: cores.corSecundaria,
    borderWidth: 1
  },
  containerIndicator:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 70
  },
  containerGoogleSignIn:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  containerListItem:{
    position: 'relative',
    flexDirection:'row',
    height:60,
    alignItems:'center',
    marginLeft:15,
    marginRight:5,
    marginBottom:10,
    flex:1
  },
  containerSearchListItem:{
    position: 'relative',
    flexDirection:'row',
    height:40,
    alignItems:'center',
    flex:1
  },
  containerListItemProdutos:{
    flex:1,
    height:60,
    justifyContent:'center'

    // alignItems:'center'
  },
  developedBy: {
    fontFamily: 'Cochin',
    fontSize: 10,
    alignSelf: 'center',
  },
  headerText:{
    flex:1,
    fontFamily: 'Futura PT Bold',
    color: cores.corSecundaria,
    textAlign: 'center',
    fontSize:wp('5%'),
    fontWeight:'200',
  },
  headerRight:{
    marginRight: wp('5.32%')
  },
  header:{
    height:hp('8%'),
    backgroundColor: cores.corPrincipal
  },
  headerList:{
    color: '#FFFFFF',
    fontSize:17,
    marginLeft:10,
    flex:1
  },
  icon: {
    width: wp('18.36%'),
    height: hp('8.5%'),
    tintColor: cores.corPrincipal
  },
  imagemEstabInfo:{
    height: 100,
    width: 100,
    alignSelf: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor:cores.corPrincipal,
    resizeMode:'cover'
  },
  imagemTipoEstabelecimento:{
    height: 60,
    width: 60,
    alignSelf: 'center',
    borderRadius: 60,
    borderWidth: 1,
    borderColor:cores.corPrincipal,
    resizeMode:'cover'
  },
  imagemListSearch:{
    height: 30,
    width: 30,
    alignSelf: 'center',
    borderRadius: 30,
    borderWidth: 1,
    borderColor:cores.corPrincipal,
    resizeMode:'cover'
  },
  imgProduto:{
    top: 10,
    marginBottom: 10,
    alignSelf: 'center',
    resizeMode: 'stretch',
    borderColor: cores.corPrincipal,
    borderWidth: 1,
    marginBottom: 15
  },
  labelCadastro: {
    opacity: 1,
  },
  logo: {
    height:hp('30%'),
    width: wp('50%'),
    marginTop: hp('28%'),
    alignSelf: 'center',
    resizeMode: 'center'
  },
  nomeAppHome:{
    alignSelf:'center',
    color: cores.corPrincipal,
    marginTop: 15,
    marginBottom: 15,
    fontSize: 20
  },
  renderSeparatorComponent:{
    height: 2,
    width: "100%",
    backgroundColor: "#CED0CE",
    marginLeft: 5,
    marginBottom: 7
  },
  renderSeparatorSection:{
    height: 2,
    width: "100%",
    backgroundColor: "#550000",
    marginLeft: 5,
    marginBottom: 7
  },
  searchBar:{
    height:60,
    color: 'rgba(0,0,0,0.5)',
  },
  searchBarContainer:{
    marginHorizontal: 10,
    borderRadius:6,
    backgroundColor:'rgba(139,0,0,0.1)'
  },
  searchBarInput:{
    fontSize: 16,
    backgroundColor: 'rgba(0,0,0,0.1)',
    color:'rgba(139,0,0,1)'
  },
  separator:{
    height:15
  },
  textAddProduto:{
    alignSelf: 'center',
    color: cores.corPrincipal,
    marginLeft:5,
    fontSize: wp('3.5%'),
    marginBottom: 15
},
textAdicionais:{
  alignSelf: 'center',
  color: cores.corPrincipal,
  fontSize: 16,
  marginBottom: 15
},
textRelatorio:{
  fontSize:wp('3.75%'),
  color:cores.corPrincipal,
  fontFamily: 'Futura Book',
  marginLeft: wp('3%'),
  marginBottom: hp('1%')
},
textRelatorioNumber:{
  fontSize:wp('3.75%'),
  color:cores.textDetalhes,
  fontFamily: 'Futura Book',
  marginBottom: hp('1%'),
},
containerTextInput:{
  borderColor:'#d1d1d1',
  borderWidth: 1,
  marginHorizontal: wp('12.31%'),
  height: hp('7%'),
  alignItems: 'center'
},
  textButtons:{
    backgroundColor:'transparent',
    alignSelf:'center',
    color:'#FFFFFF',
    fontSize: wp('4%'),
    fontFamily: 'Futura Book'
  },
  textCarrinho:{
    fontSize: wp('3.5%'),
    fontFamily: 'Futura Medium',
    marginBottom: hp('0.25%')
  },
  textCarrinhoAdicionais:{
    alignSelf: 'center',
    fontSize: 14
  },
  textDetalhesEstabelecimento:{
    marginLeft: 5,
    fontSize: 13,
    color: cores.textDetalhes
  },
  textEndHome:{
    fontSize: 13,
    marginTop:5,
    color: cores.corPrincipal,
  },
  textEstabelecimento:{
    marginLeft: 5,
    fontSize: 17,
    color: cores.corPrincipal
  },
  textInformacoes:{
    marginLeft:15,
    fontSize: 18,
    color:cores.corPrincipal
  },
  textInformacoes2:{
    marginLeft:20,
    fontSize:17,
    color:cores.textDetalhes
  },
  textInformacoesD: {
    marginLeft:30,
    fontSize: 16,
    color:cores.textDetalhes
  },
  textInputs:{
    borderColor: '#8b0000',
  },
  textPreco:{
    fontSize: 15,
    marginRight: 15,
    color: cores.corPrincipal
  },
  titleCadastro: {
    paddingBottom: 16,
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Futura PT Bold',
    fontWeight:'200',
    fontSize: wp('5.5%'),
    fontWeight: 'normal',
    opacity: 1,
  }
});
