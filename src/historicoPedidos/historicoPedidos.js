
import React, { Component } from 'react';
import { Alert,ImageBackground, FlatList, Image, View,ScrollView, Text, Button, TouchableHighlight, YellowBox,BackHandler } from 'react-native'
import { styles, cores, images} from '../constants/constants'
import LazyActivity from '../constants/lazyActivity'
import { horarioFuncionamento,loadMessages,pedidosHistorico } from '../firebase/database'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import HistoricoPedidosListItem from './historicoPedidosListItem'
import HistoricoPedidosComponents from './historicoPedidosComponents'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LazyBackButton from '../constants/lazyBackButton'

import _ from 'lodash';

let todocount=0
let listener = null
let temPedidos=''
let nomeEstab=''
var inicioA=0
var finalA=0


export class HistoricoPedidosScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    title: "RELATÓRIOS",
    headerTitleStyle: styles.headerText,
    headerStyle: styles.header,
    headerLeft: (
      <LazyBackButton
        goBack={()=>{
          navigation.navigate('Home')
          }}/>
      ),
      headerRight:(<View style={styles.headerRight}></View>)
  })

  constructor(props) {
    super(props);
    //Estados para pegar as informações do Usuário
    this.state = {
      loading:true,
      messages:[],
      refreshing: false
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

  }

  componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick=()=> {
    this.props.navigation.goBack();
    return true;
  }

  componentDidMount(){
    this.setState({
      loading:true
    });
    const {state} = this.props.navigation;
    nomeEstab = state.params ? state.params.nomeEstab : ""
    loadMessages(nomeEstab,()=>{
      temPedidos = pedidosHistorico
      if(!pedidosHistorico){
        console.log("dentro if");
        this.setState({
          messages: false
        },function(){
          this.setState({
            loading:false
          });
        });
      }else{
        var _pedidoHistorico =_.orderBy(pedidosHistorico,['createdAt'],['desc'])
      this.setState({
        messages:_pedidoHistorico
      },function(){
        console.log("tamanho"+this.state.messages.length);
        this.setState({
          loading:false
        });
      })
    }
    })
  }

  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };


  _renderItem=({item,index})=>{
    return(
      <HistoricoPedidosListItem
        onPressSend={()=>this.onPressSend(item,index)}
        item={item}>
      </HistoricoPedidosListItem>
    )
  }

  mapRelatorio=(inicio,final)=>{
    var count=0
    var debito=0
    var credito=0
    var dinheiro=0
    var debitoF=0
    var creditoF=0
    var dinheiroF=0
    var frete=0
    var produtos=[]
    console.log("inicio sem date"+inicio);
    // console.log("antes messages map"+JSON.stringify(this.state.messages));
    this.state.messages.map((item,index)=>{
      // console.log("item.nome"+item.nome);
      // console.log("item.createdAt sem date"+item.createdAt);
      // console.log("item.createdAt"+new Date(item.createdAt));
      // console.log("inicio"+new Date(inicio));
      // console.log("final"+new Date(final));
      // console.log("item.status"+item.status);
      if(item.createdAt>inicio&&item.createdAt<final&&item.status=="Pedido Enviado"){
        count++
        console.log("itens"+JSON.stringify(item));
        if(item.formaPgto=="Crédito"){
          credito=credito+item.total
          creditoF=creditoF+item.frete+item.total
        }
        if(item.formaPgto=="Débito"){
          debito=debito+item.total
          debitoF=debitoF+item.frete+item.total
        }
        if(item.formaPgto=="Dinheiro"){
          dinheiro=dinheiro+item.total
          dinheiroF=dinheiroF+item.frete+item.total
        }
        frete = frete+item.frete
        item.carrinho.map((item1,index1)=>{
          for (var i = 0; i < item1.quantidade; i++) {
            produtos.push(item1.nome)
          }
        })

      }
    })


    // console.log("valores"+JSON.stringify(_.countBy(produtos)));
    this.gerarResultado(count,debito,debitoF,credito,creditoF,dinheiro,dinheiroF,frete,produtos)
    this.setState({
      loading:false
    });
  }

  gerarResultado=(qtde,deb,debF,cre,creF,din,dinF,frete,produtos)=>{
    console.log("gerar resultado qtde"+qtde);
    console.log("gerar resultado deb"+deb);
    console.log("gerar resultado cre"+cre);
    console.log("gerar resultado din"+din);
    console.log("total"+deb+cre+din);
    console.log("gerar resultado produtos"+produtos);
    prodVendas = _.countBy(produtos)

    const itemVisto = Object.create(null);
    produtos.forEach(btn => {
        itemVisto[btn] = true;
    });

    const itensUnicos = Object.keys(itemVisto)

    this.setState({
      prodVendas: prodVendas,
      itensUnicos: itensUnicos,
      credito:cre,
      creditoF:creF,
      debito:deb,
      debitoF:debF,
      dinheiro:din,
      dinheiroF:dinF,
      frete:frete,
      qtdePedidos:qtde,
      total: cre+deb+din,
      totalF: creF+debF+dinF
    });

  }

  gerarRelatorio=(inicio,final)=>{
    this.setState({
      loading:true
    });
    //se houver algum campo Vazio
    if(inicio==null||final==null){
      Alert.alert(
        'Campo Vazio.',
        'Selecione data inicial e final para gerar o relatorio.',
        [
          {text: 'OK', onPress: () => {
            this.setState({
              loading:false
            });
          }},
        ],
        { cancelable: false }
      )
    }
    //se os dois campos estiverem preenchidos
    else{
      var tempInicio = inicio.split('/');
      var dateInicio = new Date(tempInicio[2], tempInicio[1]-1, tempInicio[0]);

      var tempFinal = final.split('/');
      var dateFinal = new Date(tempFinal[2], tempFinal[1]-1, tempFinal[0]);

      var inicioNum = Date.parse(dateInicio)
      var finalNum = Date.parse(dateFinal)

      //se a data final for menor que a data inicial
      if(finalNum<inicioNum){
        Alert.alert(
          'Data Final incorreta',
          'A data final deve ser maior ou igual à data inicial',
          [
            {text: 'OK', onPress: () => {
              this.setState({
                loading:false
              });
            }},
          ],
          { cancelable: false }
        )
      }
      //se as datas estiverem corretas
      else{
        var weekday = ["domingo","segunda","terca","quarta","quinta","sexta","sabado"]
        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"]

        var diaInicioObj = new Date(inicioNum)
        var dayIni = diaInicioObj.getDate()
        var monthIni = months[diaInicioObj.getMonth()]
        var yearIni = diaInicioObj.getFullYear()

        var diaFinalObj = new Date(finalNum)
        var dayFin = diaFinalObj.getDate()
        var monthFin = months[diaFinalObj.getMonth()]
        var yearFin = diaFinalObj.getFullYear()

        //se as datas coincidirem
        if(Date.parse(diaInicioObj)==Date.parse(diaFinalObj)){
          console.log("mesmo dia");
          let diaInicio = weekday[diaInicioObj.getDay()];


          horarioFuncionamento(nomeEstab,diaInicio,(horario)=>{

            let inicioParse = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            let finalParse = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.fechamento)

            var aberturaObject = new Date(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            var fechamentoObject = new Date(monthIni+" "+dayIni+", "+yearIni+" "+horario.fechamento)

            //se o fechamento for de madrugada adicionar um dia
            if(finalParse<inicioParse){
              let dayAfter= fechamentoObject.getDate()+1
              fechamentoObject.setDate(dayAfter)
              //atualiza fechamento para data correta
              inicioA = Date.parse(aberturaObject)
              finalA = Date.parse(fechamentoObject)
              console.log("mapRelatorio1");
              this.mapRelatorio(inicioA,finalA)
            }

            else{
              inicioA = Date.parse(aberturaObject)
              finalA = Date.parse(fechamentoObject)
              console.log("mapRelatorio2");
              this.mapRelatorio(inicioA,finalA)
            }
          })

        }
        //se as datas não coincidirem
        else{
          let diaInicio = weekday[diaInicioObj.getDay()];
          var diaFinal = weekday[diaFinalObj.getDay()];

          horarioFuncionamento(nomeEstab,diaInicio,(horario)=>{
            inicioA = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            console.log("monthIni"+monthIni);
            console.log("dayIni"+dayIni);
            console.log("yearIni"+yearIni);
            console.log("horario.abertura"+horario.abertura);
            console.log("inicioA"+inicioA);

            horarioFuncionamento(nomeEstab,diaFinal,(horario2)=>{
              let inicioParse = Date.parse(monthFin+" "+dayFin+", "+yearFin+" "+horario2.abertura)
              let finalParse = Date.parse(monthFin+" "+dayFin+", "+yearFin+" "+horario2.fechamento)

              var fechamentoObject = new Date(monthFin+" "+dayFin+", "+yearFin+" "+horario2.fechamento)

              if(finalParse<inicioParse){
                let dayAfter= fechamentoObject.getDate()+1
                fechamentoObject.setDate(dayAfter)
                //atualiza fechamento para data correta
                finalA = Date.parse(fechamentoObject)
                console.log("mapRelatorio3");
                this.mapRelatorio(inicioA,finalA)
              }else{
                finalA = Date.parse(fechamentoObject)
                console.log("mapRelatorio4");
                this.mapRelatorio(inicioA,finalA)
              }
            })
          })
        }
      }
    }
  }

  valorVirgula(valor){
    console.log("valor"+valor);
    var str = parseFloat(valor)
    str = str.toFixed(2)
    var res = str.toString().replace(".",",")
    return(
        <Text style={styles.textRelatorioNumber}>{res}</Text>
    )
  }

  render() {

    console.ignoredYellowBox = [
      'Setting a timer'
    ]
    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <LazyActivity/>
    </View> :

    <View style={{flex:1}}>
      {
        temPedidos ?
        <View style={{flex:1}}>
          <HistoricoPedidosComponents
            dataInicio={this.state.dataInicio}
            onChangeDataInicio={(date)=>{this.setState({dataInicio: date})}}
            dataFinal={this.state.dataFinal}
            onChangeDataFinal={(date)=>{this.setState({dataFinal: date})}}
            gerarRelatorio={()=>{
              this.gerarRelatorio(this.state.dataInicio,this.state.dataFinal)
            }}
            />
          {this.state.itensUnicos?
            <ScrollView>
              <Text style={{
                color:cores.corPrincipal,
                marginVertical: hp('1%'),
                alignSelf: 'center',
                fontFamily: 'Futura Book',
                fontSize: wp('4.5%'),
                }}>Resumo Vendas no Período</Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.textRelatorio}>Total de Pedidos: </Text>
                  <Text style={styles.textRelatorioNumber}>{this.state.qtdePedidos}</Text>
                </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Total em taxa de entrega: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.frete)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Total em itens: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.total)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Total geral: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.totalF)}</Text>
              </View>
              <Text style={{
                color:cores.corPrincipal,
                marginVertical: hp('1%'),
                alignSelf: 'center',
                fontFamily: 'Futura Book',
                fontSize: wp('4.5%'),
              }}>Separado por forma de pagamento</Text>
              <Text style={{
                marginVertical: hp('1%'),
                alignSelf: 'center',
                fontFamily: 'Futura Book',
                fontSize: wp('4%'),
              }}>Sem taxa de entrega: </Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Crédito: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.credito)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Débito: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.debito)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Dinheiro: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.dinheiro)}</Text>
              </View>
              <Text style={{
                marginVertical: hp('1%'),
                alignSelf: 'center',
                fontFamily: 'Futura Book',
                fontSize: wp('4%'),
              }}>Com taxa de entrega: </Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Crédito: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.creditoF)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Débito: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.debitoF)}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.textRelatorio}>Valor Dinheiro: </Text>
                <Text style={styles.textRelatorioNumber}>R$ {this.valorVirgula(this.state.dinheiroF)}</Text>
              </View>
              <View style={{marginTop:hp('2.2%'),marginBottom:hp('1.5%')}}>
                <Text style={{  alignSelf: 'center',              color:cores.corPrincipal,
                                fontFamily: 'Futura Book',
                                fontSize: wp('4.5%'),}}>Lista e quantidade de itens vendidos</Text>
              </View>
              {this.state.itensUnicos.map((item,index)=>{
                return(
                  <View key={index} style={{flexDirection: 'row'}}>
                    <Text style={styles.textRelatorio}>{item}: </Text>
                    <Text style={styles.textRelatorioNumber}>{this.state.prodVendas[item]}</Text>
                  </View>
                )
              })}
            </ScrollView>
            :
            <View>
              <Text style={{marginTop: hp('1%'),
                  alignSelf: 'center',
                fontFamily: 'Futura Book',
                color: cores.corPrincipal,
              fontSize: wp('4%')}}>Sem pedidos no período pesquisado.</Text>
            </View>
          }
        </View>

            :
        <View style={{marginTop:hp('1.11%')}}><Text style={styles.textAddProduto}>Sem pedidos realizados.</Text></View>
      }
    </View>

    return (
      <ImageBackground
        source={images.imageBackground}
        style={styles.backgroundImage}>
          {content}
      </ImageBackground>
    );
  }

}
