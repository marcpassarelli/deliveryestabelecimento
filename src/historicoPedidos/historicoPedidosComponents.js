import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Button } from 'react-native';
import { styles, cores, images} from '../constants/constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LazyYellowButton from '../constants/lazyYellowButton'
import DatePicker from 'react-native-datepicker'

export default class HistoricoPedidosComponents extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    return(
      <View>
      <Text style={{alignSelf: 'center',fontFamily: 'Futura Book',fontSize: wp('4%'),marginTop: hp('1%')}}>Escolha o período do relatório:</Text>
      <DatePicker
        style={{width: wp('70%'),alignSelf: 'center',marginTop: hp('1%')}}
        date={this.props.dataInicio}
        mode="date"
        placeholder="Selecione a data inicial"
        format="DD/MM/YYYY"
        minDate="01/01/2019"
        maxDate="31/12/2030"
        confirmBtnText="Confirmar"
        cancelBtnText="Cancelar"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36,
          },
          dateText:{
            fontFamily:'Futura Book',
            color:cores.corPrincipal,
            fontSize:wp('3.75%')
          },
          placeholderText:{
            fontFamily:'Futura Book',
            color:cores.corPrincipal,
            fontSize:wp('3.75%')
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.props.onChangeDataInicio(date)}}
      />
      <DatePicker
        style={{width: wp('70%'),alignSelf: 'center',marginTop: hp('1%')}}
        date={this.props.dataFinal}
        mode="date"
        placeholder="Selecione a data final"
        format="DD/MM/YYYY"
        minDate="01/01/2019"
        maxDate="31/12/2030"
        confirmBtnText="Confirmar"
        cancelBtnText="Cancelar"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36,
          },
          dateText:{
            fontFamily:'Futura Book',
            color:cores.corPrincipal,
            fontSize:wp('3.75%')
          },
          placeholderText:{
            fontFamily:'Futura Book',
            color:cores.corPrincipal,
            fontSize:wp('3.75%')
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.props.onChangeDataFinal(date)}}
      />
      <View style={{marginTop: hp('1%')}}>
        <LazyYellowButton
          styleButton={{width: wp('80%')}}
          styleText={{fontFamily:'Futura PT Bold',color:cores.corPrincipal, fontSize: wp('4.5%')}}
          onPress={()=>{this.props.gerarRelatorio()}}
          text={"GERAR RELATÓRIO"}
          />
      </View>
      </View>
    )
  }
}
