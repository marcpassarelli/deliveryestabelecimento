console.ignoredYellowBox = [
    'Setting a timer'
]
import { Text, View, TextInput, Image, Button, TouchableHighlight, TouchableOpacity  } from 'react-native';
import { styles, goToHome, goToCadastro } from '../constants/constants'
import LazyTextInput from '../constants/lazyTextInput'
import LazyYellowButton from '../constants/lazyYellowButton'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import React, { Component } from 'react';

export default ComponentsLogin = (props) => {
  return (
      <KeyboardAwareScrollView>
          <Text style={[styles.titleCadastro,{marginTop: hp('12%')}]}>LOGIN</Text>

          <LazyTextInput
            style={{marginBottom: hp('4.22%')}}
            placeholder={'EMAIL'}
            nameIcon={'user'}
            onChangeText = {props.updateEmail}
            returnKeyType="next"
           />
          <LazyTextInput
            nameIcon={'lock'}
            style={{marginBottom: hp('4.22%')}}
            placeholder={'SENHA'}
            onChangeText = {props.updateSenha}
            secureTextEntry = {true}
            returnKeyType="done"
          />
        <LazyYellowButton
          style={{}}
          onPress= { () => {props.loginToHome()} }
          text={'LOGIN'}
          />
    </KeyboardAwareScrollView>
  )

}
