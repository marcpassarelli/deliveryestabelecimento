
import { Madoka } from 'react-native-textinput-effects';
import ComponentsLogin from './componentslogin';
import { login, checkUserDetails,getEstabName } from '../firebase/database'
import React, { Component } from 'react';
import { ImageBackground, Image, Alert, Navigator,Text, View,TouchableOpacity, ActivityIndicator, BackHandler, Platform } from 'react-native';
import { styles,images } from '../constants/constants'
import * as firebase from 'firebase';
import {db, auth} from '../firebase/firebase'
import { NetPrinter } from 'react-native-printer';
import Loader from '../constants/loadingModal';
import { checkInternetConnection } from 'react-native-offline';

let listener = null
var count =0
export class LoginScreen extends Component {

  componentDidMount() {
    //Encerrar app se for android e se back for pressionado
    this.getUser()
    if (Platform.OS == "android" && listener == null) {
      listener = BackHandler.addEventListener("hardwareBackPress", () => {
        BackHandler.exitApp()
      })
    }

  }

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      senha: '',
      nameUser: '',
      profilePicUrl:'',
      loading: false,
      estabName:''
    }

    this.loginToHome = this.loginToHome.bind(this);

  }
  async getUser(){
      const { navigate } = this.props.navigation;
      let user = await firebase.auth().currentUser
      if(user){
        console.log("user.uid"+user.uid);
      getEstabName(user.uid,
      (estabName)=>{
        console.log("estabName"+estabName.nome);
        this.setState({
          estabName: estabName.nome,
          tipoEstab: estabName.tipo
        },function(){
          if(this.state.estabName){
            navigate('Home',{nomeEstab:this.state.estabName,tipoEstab:this.state.tipoEstab})
          }else{
            navigate('Login')
          }
        });
      })
    }
  }


  updateEmail = (text) => {
    this.setState({email: text})
  }
  updateSenha = (text) => {
    this.setState({senha: text})
  }


  async loginToHome () {
    this.setState({
      loading: true
    });
    const { navigate } = this.props.navigation;
    if(this.state.email && this.state.senha){

      const isConnected = await checkInternetConnection();

      if(isConnected){
        login(
          this.state.email,
          this.state.senha,
          async () => {
            let user = await auth.currentUser
            getEstabName(user.uid,
              (estabName)=>{
                this.setState({
                  estabName:estabName.nome,
                  tipoEstab:estabName.tipo
                },function(){
                  navigate('Home',{nomeEstab:this.state.estabName,tipoEstab:this.state.tipoEstab})
                  this.setState({
                    loading:false
                  });
                })
              }
            )
          },
        ()=>{
          this.setState({
            loading:false
          });
        })
      }else{
        this.setState({
          loading: false
        });
        Alert.alert(
          'Conexão com a Internet',
          'Aparantemente há um problema com sua conexão de internet e não conseguimos fazer o login. Cheque para haver se você possui conexão com a internet no momento e tente novamente',
          [
            {text: 'OK', onPress: () => {

            }},
          ],
          { cancelable: false }
        )
      }

    }else{
      this.setState({
        loading:false
      });
      Alert.alert(
        'Campos Vazios',
        'Preencha todos os campos para proceder com o login',
        [
          {text: 'OK', onPress: () => {

          }},
        ],
        { cancelable: false }
      )
    }
  }

  render(){
    console.ignoredYellowBox = [
        'Setting a timer'
    ]

    return (
      <ImageBackground
        source={images.backgroundLoginEmail}
        style={styles.backgroundImage}>
        <Loader
          loading={this.state.loading}
          message="Aguarde enquanto a preguiça faz o seu login"/>
        <ComponentsLogin
          updateEmail = {this.updateEmail}
          updateSenha = {this.updateSenha}
          loginToHome = {this.loginToHome}
          />
      </ImageBackground>
    )
  }
}
