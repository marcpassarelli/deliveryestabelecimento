import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Button,FlatList } from 'react-native';
import { styles, cores } from '../constants/constants'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class ReverPedidosListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status:false
    }
  }

  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };

  valorVirgula(valor){
    var str = parseFloat(valor)
    str = str.toFixed(2)
    var res = str.toString().replace(".",",")
    return(
        <Text>{res}</Text>
    )
  }

  preco=(item)=>{
    {
      var str = (item.preco*item.quantidade).toFixed(2)
      var res = str.toString().replace(".",",")
      let fontSize=0
      if(item.adicional==true){
        fontSize=wp('3.5%')
      }else{
        fontSize=wp('3.75%')
      }
      return(
          <Text style={[styles.textCarrinho,{
              color: cores.textDetalhes,
              fontFamily: "Futura Medium Italic BT",
              alignSelf: 'center', fontSize: fontSize}]}>
            R$ {res}
          </Text>
      )
    }
  }

  functionNome(item){
    if(item.tipoProduto=="Pizzas"){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesPizzas(item.obs,item.detalhes)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
      )
    }else if(item.detalhes){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesPizzas(item.obs,item.detalhes)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
      )
    }else if(item.obs){
      return(
        <TouchableOpacity onPress={()=>this.props.detalhesObs(item.obs)}>
          <Text style={[styles.textCarrinho,
              {alignSelf: 'flex-start',textDecorationLine:'underline',
              marginHorizontal: 10,
            fontSize: wp('3.5%')}]}>
          {item.nome} - {item.tipoProduto}
          </Text>
        </TouchableOpacity>
    )}else{
      return(
        <Text style={[styles.textCarrinho,
            {alignSelf: 'flex-start',
            marginHorizontal: 10,
          fontSize: wp('3.5%')}]}>
        {item.nome} - {item.tipoProduto}
        </Text>
      )
  }
  }


  functionCarrinhoListItem=(item)=>{
    // console.log("new date"+new Date());
    console.log("item._id"+item._id);
    console.log("item.id"+item.id);
    var date = new Date(item.createdAt)
    var year = date.getFullYear();
    var month = date.getMonth() + 1; // getMonth() is zero-indexed, so we'll increment to get the correct month number
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;
    hours = (hours < 10) ? '0' + (hours) : (hours);
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds: seconds;
    console.log("hours"+hours);
    console.log("minutes"+minutes);
    console.log("seconds"+seconds);
    // <Text style={[styles.textCarrinho]}>
    //   Horário do Pedido: {hours+":"+minutes+":"+seconds}
    // </Text>
    // <Text style={[styles.textCarrinho]}>
    //   Data do Pedido: {day+"/"+month+"/"+year}
    // </Text>
      return (
          <View
            style={{borderWidth: 2, marginHorizontal: 2,borderColor: cores.corPrincipal}}>
            <View style={{flexDirection: 'column', justifyContent: 'flex-start'}}>
              <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),color: cores.textDetalhes}]}>Data do Pedido: </Text>
                <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),}]}>{day+"/"+month+"/"+year}</Text>
              </View>
              <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),color: cores.textDetalhes}]}>Horário do Pedido: </Text>
                <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),}]}>{hours+":"+minutes}</Text>
              </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),color: cores.textDetalhes}]}>Nome do Cliente: </Text>
                  <Text style={[styles.textCarrinho,{marginTop: hp('0.5%'),}]}>{item.nome}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Telefone: </Text>
                  <Text style={[styles.textCarrinho]}>{item.telefone}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Endereco: </Text>
                  <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.endereco}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Bairro: </Text>
                  <Text style={[styles.textCarrinho]}>{item.bairro}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Referência: </Text>
                  <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.referencia}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Pedido:</Text>
                </View>
                {item.carrinho.map((item,index)=>{
                  if(item.adicional==false){
                    return (
                      <View style={{flex:1,
                          height:hp('6%'), flexDirection: 'row',justifyContent: 'space-between',
                          alignItems: 'center',marginHorizontal: wp('1%')}}
                          key={item._id}>

                        <View style={
                            {
                            borderColor: cores.corSecundaria,
                            borderWidth: 1,
                            height:hp('6%'),
                            justifyContent: 'center',
                            width:wp('18%')}}>
                            <Text style={[styles.textCarrinho,
                                {alignSelf: 'center',
                                marginLeft: 5,
                              fontSize: wp('3.75%')}]}>{item.quantidade}x
                            </Text>
                          </View>

                          <View style={{width:wp('54%'),height:hp('6%'), justifyContent: 'center',
                            borderColor: cores.corSecundaria,borderWidth: 1}}>
                              {this.functionNome(item)}
                          </View>

                          <View style={{width: wp('24%'),height:hp('6%'),marginRight: 10,
                            justifyContent: 'center',borderColor: cores.corSecundaria,borderWidth: 1}}>
                            {this.preco(item)}
                          </View>

                      </View>
                    );
                  }else{
                    return (
                      <View style={{flex:1,marginHorizontal: wp('1%'),
                          height:30, flexDirection: 'row',justifyContent: 'space-between',
                          alignItems: 'center'}}
                          key={item._id}>

                        <View style={
                            {backgroundColor:'rgba(252, 204, 60,0.5)',
                            borderColor: cores.corSecundaria,
                            borderWidth: 1,
                            height:30,
                            justifyContent: 'center',
                            width:wp('18%')}}>
                            <Text style={[styles.textCarrinho,
                                {alignSelf: 'center',
                                marginLeft: 5,
                              fontSize: wp('3.75%')}]}>{item.quantidade}
                            </Text>
                          </View>

                          <View style={{backgroundColor:'rgba(252, 204, 60,0.5)',width:wp('54%'),height:30, justifyContent: 'center',
                            borderColor: cores.corSecundaria,borderWidth: 1}}>
                            <Text style={[styles.textCarrinho,
                                {alignSelf: 'flex-start',
                                marginHorizontal: 10,
                              fontSize: wp('3.25%')}]}>
                            {item.nome} - {item.tipoProduto}
                            </Text>
                          </View>

                          <View style={{backgroundColor:'rgba(252, 204, 60,0.5)',width: wp('24%'),height: 30,marginRight: 10,
                            justifyContent: 'center',borderColor: cores.corSecundaria,borderWidth: 1}}>
                            {this.preco(item)}
                          </View>

                      </View>
                    );
                  }
                })}
                <View style={{flexDirection: 'row',marginLeft: wp('2%'),marginTop: hp('1%')}}>
                  <Text style={[styles.textCarrinho,{color: 'red',fontSize: wp('3.25%')}]}>IMPORTANTE: Itens sublinhados contém observações em relação ao item. Clique para verificar. </Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%'),marginTop: hp('1%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Total: </Text>
                  <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.total)}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Frete: </Text>
                  {item.retirar?
                    <Text style={[styles.textCarrinho]}>Pedido irá ser retirado no estabelecimento.</Text> :
                    <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.frete)}</Text>  }
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Total com Frete: </Text>
                  <Text style={[styles.textCarrinho]}>R$ {this.valorVirgula(item.total+item.frete)}</Text>
                </View>
                <View style={{flexDirection: 'row',marginLeft: wp('2%')}}>
                  <Text style={[styles.textCarrinho,{color: cores.textDetalhes}]}>Forma de Pagamento: </Text>
                  {item.formaPgto=="Dinheiro" ?
                    <Text style={[styles.textCarrinho,{flexWrap: 'wrap',flex:1}]}>{item.formaPgto} - Troco para: R${item.formaPgtoDetalhe}</Text> :
                    <Text style={[styles.textCarrinho]}>{item.formaPgto} - {item.formaPgtoDetalhe}</Text>
                  }
                </View>
                <Text style={[item.status=="Confirmado Recebimento" ? styles.recebidoStatus : styles.aguardandoStatus,
                  {fontSize: wp('3.25%')  , alignSelf: 'center',fontFamily: 'Futura Medium'}]}>Status do Pedido: {item.status}</Text>

            </View>
          </View>
      );
  }

  render() {
    const { item } = this.props;
    return(
      <View>
        {this.functionCarrinhoListItem(item)}
      </View>
    )
  }
}
