
import React, { Component } from 'react';
import {AsyncStorage, Vibration, FlatList, Image, ImageBackground, View, Text,Platform,Picker,PickerIOS, Button, ActivityIndicator, TouchableHighlight, YellowBox, Alert } from 'react-native'
import { styles, cores,images } from '../constants/constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { loadMessages,pedidosHistorico,horarioFuncionamento } from '../firebase/database'
import ReverPedidosListItem from './reverPedidosListItem'
import ReverPedidosComponents from './reverPedidosComponents'
import LazyActivity from '../constants/lazyActivity'
import LazyYellowButton from '../constants/lazyYellowButton'
import {auth} from '../firebase/firebase'
import _ from 'lodash';
import LazyBackButton from '../constants/lazyBackButton'
import { NetPrinter } from 'react-native-printer';
import ModalItem from '../home/modalItem'
import { checkInternetConnection } from 'react-native-offline'

let todocount=0
let listener = null
let teste=[];
let user=""
let nomeEstab =""
const detalhesModal = ""
var i=0

export class ReverPedidosScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    title: "HISTÓRICO DE PEDIDOS",
    headerTitleStyle: styles.headerText,
    headerStyle: styles.header,
    headerLeft: (
      <LazyBackButton
        goBack={()=>{
          navigation.navigate('Home')
          }}/>
      ),
      headerRight:(<View style={styles.headerRight}></View>)
  })

  constructor(props) {
    super(props);
    //Estados para pegar as informações do Usuário
    this.state = {
      loading:false,
      messages:[],
      refreshing: false,
      modalItem:false,
      estabName:"",
      statusPedido:"enviado"

    }

  }

  updateStatusPedido=(value)=>{
    this.setState({ statusPedido: value },function(){console.log("statusPedido"+this.state.statusPedido);})
  }

  showModal(obs){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {obs?<Text style={{marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  closeModal(){
    this.setState({
      modalItem:!this.state.modalItem
    });
  }

  componentDidMount(){
    this.setState({
      loading:true
    });
    const {state} = this.props.navigation;
    nomeEstab = state.params ? state.params.nomeEstab : ""
    loadMessages(nomeEstab,()=>{
      temPedidos = pedidosHistorico
      if(!pedidosHistorico){
        console.log("dentro if");
        this.setState({
          messages: false
        },function(){
          this.setState({
            loading:false
          });
        });
      }else{
        var _pedidoHistorico =_.orderBy(pedidosHistorico,['createdAt'],['asc'])
      this.setState({
        messages:_pedidoHistorico
      },function(){
        console.log("tamanho"+this.state.messages.length);
        this.setState({
          loading:false
        });
      })
    }
    })
  }


  renderSeparatorComponent = () => {
    return (<View style={styles.renderSeparatorComponent}/>)
  };

  showModal(obs){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {obs?<Text style={{fontFamily: 'Futura Book',marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }

  showModalPizza(obs,detalhes){
    this.setState({
      modalItem: !this.state.modalItem,
    });
    this.detalhesModal =
      <View>
        {detalhes?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>{detalhes}</Text>:<View></View>}
        {obs?<Text style={{fontFamily: 'Futura Book', marginHorizontal: 10,marginBottom:15 ,color:'#666666', fontSize: wp('4.5%')}}>Observação: {obs}</Text>:<View></View>}
      </View>
  }


  functionListaPedidos(){
    if(this.state.resultado){
    return(
      <FlatList
        refreshing={this.state.resultado}
        ItemSeparatorComponent={this.renderSeparatorComponent}
        data={this.state.resultado}
        extraData={this.state}
        renderItem={({item,index})=>
        <ReverPedidosListItem
          item={item}
          detalhesObs={(obs)=>{
            this.showModal(obs)
          }}
          detalhesPizzas={(obs,detalhes)=>{
            this.showModalPizza(obs,detalhes)
          }}
          >
        </ReverPedidosListItem>}
        keyExtractor={item => item._id}
        />
    )}else{
      return(
      <View>
        <Text>Sem pedidos no histórico.</Text>
      </View>)
    }
  }

  gerarRelatorio=(inicio,final,status)=>{
    this.setState({
      loading:true
    },function(){
      console.log("status gerarRelatorio: "+status);
    });
    //se houver algum campo Vazio
    if(inicio==null||final==null){
      Alert.alert(
        'Campo Vazio.',
        'Selecione data inicial e final para gerar o relatorio.',
        [
          {text: 'OK', onPress: () => {
            this.setState({
              loading:false
            });
          }},
        ],
        { cancelable: false }
      )
    }
    //se os dois campos estiverem preenchidos
    else{
      var tempInicio = inicio.split('/');
      var dateInicio = new Date(tempInicio[2], tempInicio[1]-1, tempInicio[0]);

      var tempFinal = final.split('/');
      var dateFinal = new Date(tempFinal[2], tempFinal[1]-1, tempFinal[0]);

      var inicioNum = Date.parse(dateInicio)
      var finalNum = Date.parse(dateFinal)

      //se a data final for menor que a data inicial
      if(finalNum<inicioNum){
        Alert.alert(
          'Data Final incorreta',
          'A data final deve ser maior ou igual à data inicial',
          [
            {text: 'OK', onPress: () => {
              this.setState({
                loading:false
              });
            }},
          ],
          { cancelable: false }
        )
      }
      //se as datas estiverem corretas
      else{
        var weekday = ["domingo","segunda","terca","quarta","quinta","sexta","sabado"]
        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"]

        var diaInicioObj = new Date(inicioNum)
        var dayIni = diaInicioObj.getDate()
        var monthIni = months[diaInicioObj.getMonth()]
        var yearIni = diaInicioObj.getFullYear()

        var diaFinalObj = new Date(finalNum)
        var dayFin = diaFinalObj.getDate()
        var monthFin = months[diaFinalObj.getMonth()]
        var yearFin = diaFinalObj.getFullYear()

        //se as datas coincidirem
        if(Date.parse(diaInicioObj)==Date.parse(diaFinalObj)){
          console.log("mesmo dia");
          let diaInicio = weekday[diaInicioObj.getDay()];


          horarioFuncionamento(nomeEstab,diaInicio,(horario)=>{

            let inicioParse = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            let finalParse = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.fechamento)

            var aberturaObject = new Date(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            var fechamentoObject = new Date(monthIni+" "+dayIni+", "+yearIni+" "+horario.fechamento)

            //se o fechamento for de madrugada adicionar um dia
            if(finalParse<inicioParse){
              let dayAfter= fechamentoObject.getDate()+1
              fechamentoObject.setDate(dayAfter)
              //atualiza fechamento para data correta
              inicioA = Date.parse(aberturaObject)
              finalA = Date.parse(fechamentoObject)
              console.log("mapRelatorio1");
              this.mapRelatorio(inicioA,finalA,status)
            }

            else{
              inicioA = Date.parse(aberturaObject)
              finalA = Date.parse(fechamentoObject)
              console.log("mapRelatorio2");
              this.mapRelatorio(inicioA,finalA,status)
            }
          })

        }
        //se as datas não coincidirem
        else{
          let diaInicio = weekday[diaInicioObj.getDay()];
          var diaFinal = weekday[diaFinalObj.getDay()];

          horarioFuncionamento(nomeEstab,diaInicio,(horario)=>{
            inicioA = Date.parse(monthIni+" "+dayIni+", "+yearIni+" "+horario.abertura)
            console.log("monthIni"+monthIni);
            console.log("dayIni"+dayIni);
            console.log("yearIni"+yearIni);
            console.log("horario.abertura"+horario.abertura);
            console.log("inicioA"+inicioA);

            horarioFuncionamento(nomeEstab,diaFinal,(horario2)=>{
              let inicioParse = Date.parse(monthFin+" "+dayFin+", "+yearFin+" "+horario2.abertura)
              let finalParse = Date.parse(monthFin+" "+dayFin+", "+yearFin+" "+horario2.fechamento)

              var fechamentoObject = new Date(monthFin+" "+dayFin+", "+yearFin+" "+horario2.fechamento)

              if(finalParse<inicioParse){
                let dayAfter= fechamentoObject.getDate()+1
                fechamentoObject.setDate(dayAfter)
                //atualiza fechamento para data correta
                finalA = Date.parse(fechamentoObject)
                console.log("mapRelatorio3");
                this.mapRelatorio(inicioA,finalA,status)
              }else{
                finalA = Date.parse(fechamentoObject)
                console.log("mapRelatorio4");
                this.mapRelatorio(inicioA,finalA,status)
              }
            })
          })
        }
      }
    }
  }

  mapRelatorio=(inicio,final,status)=>{
    var count=0
    var resultado=[]
    console.log("status: "+status);
    // console.log("antes messages map"+JSON.stringify(this.state.messages));
    if(status=="enviado"){
        console.log("enviado");
      this.state.messages.map((item,index)=>{
        if(item.createdAt>inicio && item.createdAt<final && item.status=="Pedido Enviado"){
          count++
          resultado.push(item)
        }
      })
    }else if (status=="cancelado") {
      console.log("cancelado");
      this.state.messages.map((item,index)=>{
        if(item.createdAt>inicio && item.createdAt<final && item.status=="Pedido Cancelado"){
          count++
          resultado.push(item)
        }
      })
    }else if (status=="todos") {
      console.log("todos");
      this.state.messages.map((item,index)=>{
        if(item.createdAt>inicio && item.createdAt<final){
          count++
          resultado.push(item)
        }
      })
    }


    // console.log("valores"+JSON.stringify(_.countBy(produtos)));
    this.setState({
      resultado:resultado,
      loading:false
    },function(){
      console.log("resultado "+JSON.stringify(this.state.resultado));
    });
  }

  render() {

    console.ignoredYellowBox = [
      'Setting a timer'
    ]

    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <LazyActivity/>
    </View> :

    <View style={{flex:1}}>
    <ReverPedidosComponents
      dataInicio={this.state.dataInicio}
      onChangeDataInicio={(date)=>{this.setState({dataInicio: date})}}
      dataFinal={this.state.dataFinal}
      onChangeDataFinal={(date)=>{this.setState({dataFinal: date})}}
      gerarRelatorio={()=>{
        this.gerarRelatorio(this.state.dataInicio,this.state.dataFinal,this.state.statusPedido)
      }}
      statusPedido={this.state.statusPedido}
      updateStatusPedido={this.updateStatusPedido}
      />
      {this.functionListaPedidos()}
    </View>


    return (
      <ImageBackground
        source={images.imageBackground}
        style={styles.backgroundImage}>
          <ModalItem
            detalhes={this.detalhesModal}
            loading = {this.state.modalItem}
            showModal = {()=>{this.closeModal()}}/>
          {content}
      </ImageBackground>
    );
  }

}
//

// <LazyYellowButton style={{alignContent: 'flex-end',marginBottom: hp('2%')}}
//   onPress={()=>this.openEstabelecimento()}
//   text={this.state.aberto?'FECHAR':'ABRIR'}/>
