console.ignoredYellowBox = [
    'Setting a timer'
]
import React, { Component } from 'react';
import { ImageBackground, Text,Image, Animated, View, YellowBox } from 'react-native';
import FadeInOutView from '../animation/fadeinoutview'
import { styles,cores,images } from '../constants/constants'
import * as firebase from 'firebase';
import { getEstabName } from '../firebase/database'
/*import { Home } from './home'*/
export class SplashScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loggedIn:'',
      estabName:'',
      tipoEstab:''
    };
  }
  validateUserName(){
    console.log("setfState:"+this.state.loggedIn)
    return this.state.loggedIn+""
  }

  async getUser(){
    const { navigate } = this.props.navigation;
    let user = await firebase.auth().currentUser
    if(user){
      console.log("user.uid"+user.uid);
    getEstabName(user.uid,
    (estabName)=>{
      console.log("estabName"+estabName.nome);
      this.setState({
        estabName: estabName.nome,
        tipoEstab: estabName.tipo
      },function(){
        if(this.state.estabName){
          navigate('Home',{nomeEstab:this.state.estabName,tipoEstab:this.state.tipoEstab})
        }else{
          navigate('Login')
        }
      });
    })
  }else{
    navigate('Login')
  }


  }

  componentWillMount() {
      setTimeout (() => {
        this.getUser()
      }, 2000);
  }

  static navigationOptions = {
     header: null,
  };

   render() {
     console.ignoredYellowBox = [
       'Setting a timer'
     ]
     return(
       <ImageBackground
         source={images.backgroundSplash}
         style={styles.backgroundImage}>
       <FadeInOutView style={{justifyContent: 'center',alignContent: 'center'}}>
         <Image
          style={styles.logo}
          source={images.iconSplash}
         />
       <Text style={styles.developedBy}>PARA ESTABELECIMENTOS</Text>
       <Text style={styles.developedBy}>desenvolvido por Marc Passarelli</Text>
       </FadeInOutView>
       </ImageBackground>
     );
   }
}
