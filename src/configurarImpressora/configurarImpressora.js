console.ignoredYellowBox = [
    'Setting a timer'
]
import React, { Component } from 'react';
import { ImageBackground, Image, Text, TouchableOpacity, View,BackHandler,AsyncStorage, Picker } from 'react-native';
import { styles, images} from '../constants/constants'
import LazyTextInput from '../constants/lazyTextInput'
import LazyYellowButton from '../constants/lazyYellowButton'
import LazyActivity from '../constants/lazyActivity'
import LazyBackButton from '../constants/lazyBackButton'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export class ConfigurarImpressoraScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: "Configurar Impressoras",
    headerTitleStyle: [styles.headerText,{alignSelf:'center'}],
    headerStyle: styles.header,
    headerLeft: (
      <View style={{flex:1}}>
      <LazyBackButton
        goBack={()=>{
        navigation.navigate('Home')
      }}/>
      </View>
    ),
    headerRight:(<View></View>),

  });

  constructor(props) {
    super(props);
    this.state = {
         ip: '',
         porta: '',
         nome: '',
         qtdeImpressoes:'',
         tipoImpressora:''
      }

      this.cadastrarImpressora = this.cadastrarImpressora.bind(this);
  }

  updateNome = (text) => {
    this.setState({nome: text})
  }

  updateIp = (text) => {
    this.setState({ip: text})
  }
  updatePorta = (text) => {
    this.setState({porta: text})
  }
  updateQtdeImpressoes = (text) => {
    this.setState({qtdeImpressoes: text})
  }

  componentDidMount(){

    this.setState({
      loading: true
    }, async function(){
      var qtdeImpressoes = await AsyncStorage.getItem('qtdeImpressoes')
      if(!qtdeImpressoes){
        qtdeImpressoes = 0
      }else{
        this.setState({
          qtdeImpressoes:qtdeImpressoes
        });
      }

      const impressoras = await AsyncStorage.getItem('impressoras')
      let novaImpressora = JSON.parse(impressoras);

      if( !novaImpressora ){
        console.log("nenhuma impressora");
        novaImpressora = []
      }

      this.setState({
        listaImpressoras: novaImpressora
      },function(){
        this.setState({
          loading:false
        });
      });
    });

  }

   async cadastrarImpressora () {
     this.setState({
       loading:true
     });
     const {navigate} = this.props.navigation

     if(this.state.nome && this.state.ip && this.state.porta){


      const impressoraNova = { 'nome' : this.state.nome, 'ip' : this.state.ip, 'porta' : this.state.porta}

      let novaImpressora = this.state.listaImpressoras

      novaImpressora.push( impressoraNova )


       await AsyncStorage.setItem('impressoras', JSON.stringify(novaImpressora) )
         .then( ()=>{
           this.setState({
             listaImpressoras: novaImpressora,
             loading : false
           });
         console.log("It was saved successfully")
         } )
         .catch( ()=>{
         console.log("There was an error saving the product")
         } )

      }else{
        this.setState({
          loading : false
        });
        alert('Preencha todos os campos')
      }
    }

    async cadastrarQtdeImpressoes(){
      await AsyncStorage.setItem('qtdeImpressoes', this.state.qtdeImpressoes)
        .then( ()=>{
        alert('Quantide de impressões por pedido atualizada.')
        } )
        .catch( ()=>{
        console.log("There was an error saving the product")
        } )
    }

    //callback to wait for the user to be found
    validateUserName(){
      return this.state.ip+""
    }

    handleBackButtonClick=()=> {
      this.props.navigation.goBack();
      return true;
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
      const {state} = this.props.navigation;

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    updateTipoImpressora = (tipoImpressora) => {
   this.setState({ tipoImpressora: tipoImpressora })
}

  render(){
    console.ignoredYellowBox = [
        'Setting a timer'
    ]

    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <LazyActivity/>
    </View> :
    <View>
      <Text style={[styles.titleCadastro,{marginVertical: hp('1%')}]}>CADASTRE UMA IMPRESSORA</Text>
          <LazyTextInput
            style={{marginBottom: hp('3%')}}
            nameIcon={'printer'}
            placeholder={'NOME IMPRESSORA'}
            onChangeText = {this.updateNome}
            returnKeyType="next"
            autoCapitalize="words"
            value = {this.state.nome}
          />
          <LazyTextInput
            style={{marginBottom: hp('3%')}}
            nameIcon={'printer'}
            placeholder={'IP'}
            onChangeText = {this.updateIp}
            keyboardType="numeric"
            returnKeyType="next"
            value = {this.state.ip}
          />
          <LazyTextInput
            style={{marginBottom: hp('3%')}}
            nameIcon={'printer'}
            placeholder={'PORTA'}
            onChangeText = {this.updatePorta}
            returnKeyType="done"
            keyboardType="numeric"
            value = {this.state.porta}
          />
      <LazyYellowButton
        onPress = { () => {this.cadastrarImpressora()} }
        text={"CADASTRAR IMPRESSORA"}/>

      <View>
        <Text style={{marginTop: 30,marginBottom: 10,fontFamily: 'Futura Book', fontSize:wp('5%'),
          color: 'white',textAlign: 'center'}}>Impressões por pedido:</Text>
          <LazyTextInput
            style={{marginBottom: hp('3%')}}
            nameIcon={'printer'}
            placeholder={'IMPRESSÕES POR PEDIDO'}
            onChangeText = {this.updateQtdeImpressoes}
            returnKeyType="done"
            keyboardType="numeric"
            value = {this.state.qtdeImpressoes}
          />
      </View>

      <LazyYellowButton
        onPress = { () => {this.cadastrarQtdeImpressoes()} }
        text={"SALVAR QUANTIDADE DE IMPRESSOES"}/>

      <Text style={{marginTop: 30,fontFamily: 'Futura Book', fontSize:wp('5%'),
        textAlign: 'center',color: 'white'}}>Impressoras Cadastradas</Text>
      {
        this.state.listaImpressoras?
        this.state.listaImpressoras.map((item,index)=>{
          console.log("dentromap");
          return(
            <View key={index} style={{marginLeft: 20, marginVertical: 5}}>
              <Text style={{fontFamily: 'Futura Book', fontSize: wp('4%')}}>Nome: {item.nome}</Text>
              <Text style={{fontFamily: 'Futura Book', fontSize: wp('4%')}}>IP: {item.ip}</Text>
              <Text style={{fontFamily: 'Futura Book', fontSize: wp('4%')}}>Porta: {item.porta}</Text>
              <TouchableOpacity onPress={async ()=>{
                  let impressoras = this.state.listaImpressoras
                  impressoras.splice(index,1)
                  await AsyncStorage.setItem('impressoras', JSON.stringify(impressoras) )
                    .then( ()=>{
                      this.setState({
                        listaImpressoras: impressoras,
                        loading : false
                      });
                    console.log("It was saved successfully")
                    } )
                    .catch( ()=>{
                    console.log("There was an error saving the product")
                    })
                }}>
                <Text style={{fontFamily: 'Futura Book', fontSize: wp('4%'),color:'red'}}>Excluir</Text>
              </TouchableOpacity>
            </View>
          )
        })
        :
          <View>
            <Text>Sem impressoras cadastradas</Text>
          </View>
      }

    </View>

    return (
      <ImageBackground
        source={images.backgroundLazy}
        style={styles.backgroundImage}>
        <KeyboardAwareScrollView>
          {content}
        </KeyboardAwareScrollView>
      </ImageBackground>
    )
  }
}
