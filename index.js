import React from 'react';
import {
  AppRegistry, Easing, Animated
} from 'react-native';
import { createStackNavigator , TabNavigator,createBottomTabNavigator } from 'react-navigation';
import { SplashScreen } from './src/splash/splash'
import { LoginScreen } from './src/login/login'
import { HomeScreen } from './src/home/home'
import { PreparandoScreen } from './src/home/preparando'
import { ProfileScreen } from './src/home/profile'
import { HistoricoPedidosScreen } from './src/historicoPedidos/historicoPedidos'
import { ReverPedidosScreen } from './src/reverPedidos/reverPedidos'
import { ConfigurarImpressoraScreen } from './src/configurarImpressora/configurarImpressora'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { styles, cores } from './src/constants/constants'
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const TabHome = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
  },
  Preparando:{
    screen: PreparandoScreen,
  },
  Profile: {
    screen: ProfileScreen,
  },
}, {
  tabBarOptions: {
    activeTintColor: cores.corPrincipal,
    labelStyle: {
      fontSize: wp('3.75%'),
      fontFamily:'FuturaBookCTT Normal',
      color:cores.corPrincipal,

    },
    style: {
      backgroundColor: cores.corSecundaria,
      height:hp('10%'),

    },
    showIcon: true,
    indicatorStyle:{
      backgroundColor:cores.corPrincipal,
    }
  },
  navigationOptions: {
    title: 'NoHeader!',
    header: null
  },
  tabBarPosition:'bottom',
  lazy:true
})

TabHome.navigationOptions = {
  header: null
}

const DeliveryEstabelecimento = createStackNavigator ({
  Splash: { screen: SplashScreen },
  Login: { screen: LoginScreen },
  Home: { screen: TabHome },
  ReverPedidos: { screen: ReverPedidosScreen },
  HistoricoPedidos: { screen: HistoricoPedidosScreen },
  ConfigurarImpressora : { screen: ConfigurarImpressoraScreen }

},
  { headerMode: 'float',},

);

console.ignoredYellowBox = ['Warning: BackAndroid']


AppRegistry.registerComponent('DeliveryEstabelecimento', () => DeliveryEstabelecimento);
